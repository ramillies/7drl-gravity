import std.algorithm;
import std.array;
import std.conv;
import std.math;
import std.range;
import std.stdio;

import sfmlish.system.vec;
import sfmlish.system.rect;
import sfmlish.window.keyboard;
import sfmlish.graphics.sprite;
import sfmlish.graphics.color;
import entitysysd;

import entities, components;
import util;

enum runMixin =
	`override void run(EntityManager entities, EventManager events, Duration dt) {` ~
	`action(entities, events, dt.total!"hnsecs" * 1e-7);` ~
	`}`;


void runAllSystems(EntitySysD ecs, double dt)
{
	ecs.systems.run(hnsecs(to!long(dt*1e7)));

}

void addSystems(T...)(EntitySysD ecs, T systems)
{
	foreach(s; systems)
		ecs.systems.register(s);
}

class SpriteUpdateSystem: System
{
	mixin(runMixin);
	void action(EntityManager em, EventManager _, double dt)
	{
		foreach(e, position, tile; em.entitiesWith!(Position,Graphic))
		{
			if(!e.has!EntitySprite)
				e.register!EntitySprite(new Sprite);
			auto sprite = e.component!EntitySprite.spr;
			sprite.pos = position.pos;
			sprite.texture = tile.texture;
			sprite.tilenumber = tile.tile;
			sprite.origin = tile.origin;

			Color colorize = e.has!Colorization ? e.component!Colorization.color : Color.White;
			if(e.has!Transparency) colorize.a = to!ubyte(255*e.component!Transparency.transparency);
			sprite.color = colorize;
		}
	}
}

class PlayerKeyboardMoveSystem: System
{
	mixin(runMixin);
	void action(EntityManager em, EventManager _, double dt)
	{
		foreach(e, speed, _; em.entitiesWith!(MovementSpeed,Player))
		{
			e.ensureHas!Velocity;
			e.component!Velocity.velocity = speed.speed * (
				(Keyboard.keyPressed(Key.W) ? Vec2(0,-1) : Vec2(0,0)) + 
				(Keyboard.keyPressed(Key.A) ? Vec2(-1,0) : Vec2(0,0)) + 
				(Keyboard.keyPressed(Key.S) ? Vec2(0,1) : Vec2(0,0)) + 
				(Keyboard.keyPressed(Key.D) ? Vec2(1,0) : Vec2(0,0))
			);
	}
	}
}

class VelocitySystem: System
{
	mixin(runMixin);
	void action(EntityManager em, EventManager _, double dt)
	{
		foreach(e, pos; em.entitiesWith!Position)
		{
			Vec2 velocity = Vec2(0,0);
			if(e.has!Velocity)
				velocity = velocity + e.component!Velocity.velocity; 
			if(e.has!GravityVelocity)
				velocity = velocity + e.component!GravityVelocity.velocity;
			if(e.has!ForcedMoveVelocity)
				velocity = velocity + e.component!ForcedMoveVelocity.velocity;

			if(e.has!RotateInMoveDirection && e.has!EntitySprite)
				e.component!EntitySprite.spr.rotation = velocity.angle;
			pos.pos = pos.pos + velocity * dt;
		}
	}
}

class AttackSystem: System
{
	mixin(runMixin);
	void action(EntityManager em, EventManager _, double dt)
	{
		Vec2 playerPos;
		foreach(e, player, pos; em.entitiesWith!(Player, Position))
			playerPos = pos.pos;

		foreach(e, position, attack; em.entitiesWith!(Position, AttackSimple))
		{
			if(e.has!AttackOnCooldown) continue;
			Entity shot = em.objectFromTemplate(attack.projectile);
			Vec2 direction = (playerPos - position.pos).normalized;
			Vec2 newPos = position.pos;

			shot.addComponents(
				Position(newPos),
				ForcedMoveVelocity((playerPos - position.pos).normalized * attack.speed),
				EntitySprite(new Sprite)
			);

			if(e.has!Hitbox && shot.has!Hitbox)
			{
				auto rotatedHitbox = shot.component!Hitbox.vertices.map!((v) => v.rotated(direction.angle));
				double displacement = rotatedHitbox.map!((v) => v.dot(direction)).maxElement -
					e.component!Hitbox.vertices.map!((v) => v.dot(direction)).minElement;
				newPos = newPos + direction * (displacement + 10);

				Vec2[] shooterHitbox = e.component!Hitbox.vertices;
				Vec2[] shotHitbox = shot.component!Hitbox.vertices;
				Vec2 shooterDiagonal = shooterHitbox[2] - shooterHitbox[0];
				Vec2 shotDiagonal = shotHitbox[2] - shotHitbox[0];

				shot.register!MissileShooterProtection(e,
					max(abs(shooterDiagonal.x), abs(shooterDiagonal.y)) +
					max(abs(shotDiagonal.x), abs(shotDiagonal.y))
				);
			}
			e.register!AttackOnCooldown(attack.cooldown);
		}
	}
}

class RemoveShooterProtectionSystem: System
{
	mixin(runMixin);
	void action(EntityManager em, EventManager _, double dt)
	{
		foreach(e, position, protection; em.entitiesWith!(Position, MissileShooterProtection))
		{
			Entity shooter = protection.shooter;
			if(!shooter.valid || !shooter.has!Position ||
				(shooter.component!Position.pos - position.pos).abs > protection.distance
			)
				e.unregister!MissileShooterProtection;
		}
	}
}

class CooldownSystem: System
{
	mixin(runMixin);
	void action(EntityManager em, EventManager _, double dt)
	{
		foreach(e, cooldown; em.entitiesWith!AttackOnCooldown)
			if(cooldown.cooldown < dt)
				e.unregister!AttackOnCooldown;
			else
				e.component!AttackOnCooldown.cooldown -= dt;
	}
}

class AutomaticHitboxSystem: System
{
	mixin(runMixin);
	void action(EntityManager em, EventManager _, double dt)
	{
		foreach(e; em.entitiesWith!(EntitySprite, HitboxFromSprite))
		{
			Sprite sprite = e.component!EntitySprite.spr;
			Rect boundingBox = sprite.bounds;

			e.ensureHas!Hitbox;
			e.component!Hitbox.vertices = [ Vec2(0,0), Vec2(0,1), Vec2(1,1), Vec2(1,0) ].map!((v) => sprite.matrix * boundingBox.relativePoint(v)).array;
		}
	}
}

class TransformHitboxesSystem: System
{
	mixin(runMixin);
	void action(EntityManager em, EventManager _, double dt)
	{
		foreach(e, sprite, hitbox; em.entitiesWith!(EntitySprite, Hitbox))
		{
			e.ensureHas!TransformedHitbox;
			e.component!TransformedHitbox.vertices = hitbox.vertices.map!((v) => sprite.spr.matrix * (v + sprite.spr.origin)).array;
		}
	}
}

class CollisionSystem: System
{
	mixin(runMixin);
	bool checkCollision(Sprite missileSprite, Vec2[] missileHitbox, Sprite targetSprite, Vec2[] targetHitbox)
	{
		return missileSprite.bounds.intersects(targetSprite.bounds) &&
			rectsIntersecting(missileHitbox, targetHitbox);
	}

	void performCollision(Entity missile, Entity target)
	{
		if(missile.has!MissileShooterProtection && missile.component!MissileShooterProtection.shooter == target)
			return;

		if(missile.has!OnHitDamage)
		{
			if(target.has!HitPoints)
				target.component!HitPoints.current -= missile.component!OnHitDamage.power;

			missile.destroy;
		}
	}

	void action(EntityManager em, EventManager _, double dt)
	{
		foreach(missile, missileHitbox, missileSprite, _; em.entitiesWith!(TransformedHitbox, EntitySprite, Missile))
		{
			foreach(target, targetHitbox, targetSprite, _; em.entitiesWith!(TransformedHitbox, EntitySprite, Player))
				if(checkCollision(missileSprite.spr, missileHitbox.vertices, targetSprite.spr, targetHitbox.vertices))
					performCollision(missile, target);
			foreach(target, targetHitbox, targetSprite, _; em.entitiesWith!(TransformedHitbox, EntitySprite, Monster))
				if(checkCollision(missileSprite.spr, missileHitbox.vertices, targetSprite.spr, targetHitbox.vertices))
					performCollision(missile, target);
		}
	}
}

class GravitySystem: System
{
	mixin(runMixin);
	private double kappa;

	this(double k = 500^^2) { kappa = k; } 

	void action(EntityManager em, EventManager _, double dt)
	{
		double[] powers;
		Vec2[] positions;

		foreach(source, gravity, position; em.entitiesWith!(GravityWell, Position))
		{
			powers ~= gravity.power;
			positions ~= position.pos;
		}

		foreach(e, charge, position; em.entitiesWith!(GravityCharge, Position))
		{
			e.ensureHas!GravityVelocity;
			foreach(power, sourcePos; lockstep(powers, positions))
			{
				Vec2 delta = sourcePos - position.pos;
				e.component!GravityVelocity.velocity += dt * kappa * charge.charge * power * delta / delta.abs^^3;
			}
		}
	}
}

class ForcedMoveDecaySystem: System
{
	mixin(runMixin);
	void action(EntityManager em, EventManager _, double dt)
	{
		foreach(e, velocity; em.entitiesWith!ForcedMoveVelocity)
		{
			auto speed = velocity.velocity.abs;
			if(speed < velocity.decay * dt)
				e.unregister!ForcedMoveVelocity;
			else
				velocity.velocity = (speed - velocity.decay*dt) * velocity.velocity.normalized;
		}
	}
}

class GravityWellUpdateSystem: System
{
	mixin(runMixin);
	void action(EntityManager em, EventManager _, double dt)
	{
		foreach(e, gravity; em.entitiesWith!GravityWell)
		{
			e.ensureHas!Transparency;
			gravity.power = clamp(gravity.power - gravity.decay*dt, 0, 100);
			e.component!Transparency.transparency = gravity.power/100;
		}
	}
}
