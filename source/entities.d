import std.stdio;

import sfmlish.game.config;
import dyaml;
import entitysysd;

import components;

Entity entityFromYaml(EntityManager manager, Node yamlNode)
{
	if(yamlNode.type != NodeType.mapping)
		throw new EntityException("Attempted to load an entity from YAML which is not a mapping.");
	
	auto entity = manager.create;
	foreach(string componentName, Node componentParams; yamlNode)
	{
		componentSwitch: switch(componentName)
		{
			static foreach(possibleComponent; allComponents)
			{
				case possibleComponent.stringof:
					entity.register!possibleComponent;
					entity.component!possibleComponent = yamlToStruct!possibleComponent(componentParams);
					break componentSwitch;
			}

			default:
				writefln("Skipping an unknown component %s => %s", componentName, componentParams);
		}
	}

	return entity;
}

Entity objectFromTemplate(EntityManager manager, string name)
{
	return manager.entityFromYaml(Config.rawConfig("objects")[name]);
}

bool has(C)(Entity e) { return e.isRegistered!C; }
void ensureHas(C)(Entity e) { if(!e.has!C) e.register!C; }

void addComponents(T...)(Entity e, T args)
{
	foreach(arg; args)
	{
		e.register!(typeof(arg));
		e.component!(typeof(arg)) = arg;
	}
}
