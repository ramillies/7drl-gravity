import std.conv;
import std.format;

import sfmlish;
import entitysysd;

import entities, components, systems;

class TestScreen: Screen
{
	private AppWindow win;

	private EntitySysD ecs;
	private Entity player, necro;

	private bool makingGravityWell;
	private Entity currentGravityWell;

	private Text playerHP, necroHP;

	this() { }
	override void setWindow(AppWindow w) { win = w; }
	override void initialize()
	{
		ecs = new EntitySysD;
		ecs.addSystems(
			new RemoveShooterProtectionSystem,
			new AttackSystem,
			new CooldownSystem,
			new PlayerKeyboardMoveSystem,
			new GravitySystem,
			new VelocitySystem,
			new SpriteUpdateSystem,
			new AutomaticHitboxSystem,
			new TransformHitboxesSystem,
			new CollisionSystem,
			new ForcedMoveDecaySystem,
			new GravityWellUpdateSystem
		);

		player = ecs.entities.objectFromTemplate("player");
		player.register!Position(Vec2(500,400));
		necro = ecs.entities.objectFromTemplate("necro");
		necro.register!Position(Vec2(1200,200));

		with(playerHP = new Text)
		{
			font = "text";
			color = Color.White;
			pos = Vec2(100, 100);
			text = () => format("%s / %s", player.component!HitPoints.current, player.component!HitPoints.max);
		}

		with(necroHP = new Text)
		{
			font = "text";
			color = Color.White;
			pos = Vec2(1910, 10);
			relativeOrigin = Vec2(1, 0);
			text = () => format("%s / %s", necro.component!HitPoints.current, necro.component!HitPoints.max);
		}
	}

	override void event(Event e)
	{
		e.handle!(
			(OnClose _) => win.close,
			(OnResize r) { win.view = new View(r.viewport); },
			(OnMousePress m)
			{
				if(m.button == MouseButton.Right)
				{
					makingGravityWell = true;
					currentGravityWell = ecs.entities.objectFromTemplate("gravityWell");
					currentGravityWell.addComponents(
						Position(m.pos),
						GravityWell(5, -15)
					);
				}
			},
			(OnMouseMove m)
			{
				if(makingGravityWell)
				{
					currentGravityWell.component!Position.pos = m.pos;
				}
			},
			(OnMouseRelease m)
			{
				if(m.button == MouseButton.Right)
				{
					makingGravityWell = false;
					currentGravityWell.component!GravityWell.decay = 4;
				}
			}

		);
	}

	override void update(double dt) { ecs.runAllSystems(dt); playerHP.updateProperties; necroHP.updateProperties;  }
	override void updateInactive(double dt) { }
	override void draw()
	{
		foreach(entity, sprite; ecs.entities.entitiesWith!EntitySprite)
		{
			win.draw(sprite.spr);
		}
		foreach(entity, bbox; ecs.entities.entitiesWith!TransformedHitbox)
		{
			ConvexShape s = new ConvexShape;
			s.points = bbox.vertices;
			s.outlineThickness = 2;
			s.color = Color.None;
			s.outlineColor = Color.Red;
			win.draw(s);
		}

		win.draw(playerHP);
		win.draw(necroHP);
	}
	override void finish() { }
}
