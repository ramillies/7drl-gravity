import std.algorithm;
import std.datetime;
import std.file;
import std.stdio;
import std.path;
import std.range;

import sfmlish;
import entitysysd;
import dyaml;

import entities, components, systems;
import testscreen;
import util;

enum masterConfig = `
textures: textures.yaml
fonts: fonts.yaml
objects: objects.yaml
`;

void main()
{
	auto dir = new GameDataDirectory(buildPath(thisExePath.dirName.absolutePath, "data"));
	GameData.initialize(dir);
	Config.parseMasterConfig(masterConfig);
	Textures.initialize;
	Textures.load("textures");
	Fonts.initialize;
	Fonts.load("fonts");

	auto win = new AppWindow(VideoMode.desktopMode, "Test", WindowStyle.None);
	win.pushScreen(new TestScreen);
	Mainloop.addWindow(win);
	Mainloop.run;
}
