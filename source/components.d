import std.meta;
import std.stdio;

import sfmlish.system.vec;
import sfmlish.graphics.sprite;
import sfmlish.graphics.color;

import entitysysd;
import dyaml;

@component struct Position { Vec2 pos = Vec2(0,0); }
@component struct Velocity { Vec2 velocity = Vec2(0,0); }
@component struct GravityVelocity { Vec2 velocity = Vec2(0,0); }
@component struct ForcedMoveVelocity { Vec2 velocity = Vec2(0,0); double decay = 0; }
@component struct Graphic { string texture; int tile = 0; Vec2 origin = Vec2(0,0); }
@component struct HitPoints { int current, max; }
@component struct GravityCharge { double charge; }
@component struct GravityWell { double power, decay; }
@component struct MovementSpeed { double speed; }

@component struct Transparency { double transparency; }
@component struct Colorization { Color color; }

@component struct Player { }
@component struct Monster { }
@component struct Missile { }

@component struct RotateInMoveDirection { }
@component struct Hitbox { Vec2[] vertices; }
@component struct HitboxFromSprite { }

@component struct AttackSimple { string projectile; double speed, cooldown; }
@component struct AttackOnCooldown { double cooldown; }

@component struct OnHitDamage { int power; }

@component struct EntitySprite { Sprite spr; }
@component struct TransformedHitbox { Vec2[] vertices; }
@component struct SkipFrame { }
@component struct MissileShooterProtection { Entity shooter; double distance; }

alias allComponents = AliasSeq!(
	Position,
	Velocity,
	GravityVelocity,
	ForcedMoveVelocity,
	Graphic,
	HitPoints,
	GravityCharge,
	GravityWell,
	MovementSpeed,
	Player, Monster, Missile,
	RotateInMoveDirection,
	Hitbox,
	HitboxFromSprite,
	AttackSimple,
	AttackOnCooldown,
	OnHitDamage
);
