module sfmlish.graphics.color;

import sfmlish.csfml;

struct Color
{
	ubyte r, g, b, a = 255;

	static Color Black = Color.fromSfColor(sfBlack);
	static Color White = Color.fromSfColor(sfWhite);
	static Color Red = Color.fromSfColor(sfRed);
	static Color Green = Color.fromSfColor(sfGreen);
	static Color Blue = Color.fromSfColor(sfBlue);
	static Color Yellow = Color.fromSfColor(sfYellow);
	static Color Magenta = Color.fromSfColor(sfMagenta);
	static Color Cyan = Color.fromSfColor(sfCyan);
	static Color None = Color.fromSfColor(sfTransparent);

	sfColor toSfColor() const { return sfColor(r, g, b, a); }
	static Color fromSfColor(sfColor color) { return Color(color.r, color.g, color.b, color.a); }

	Color opBinary(string op)(Color rhs) const
	if(op == "+")
	{
		return Color.fromSfColor(sfColor_add(toSfColor, rhs.toSfColor));
	}

	Color opBinary(string op)(Color rhs) const
	if(op == "-")
	{
		return Color.fromSfColor(sfColor_subtract(toSfColor, rhs.toSfColor));
	}

	Color opBinary(string op)(Color rhs) const
	if(op == "*")
	{
		return Color.fromSfColor(sfColor_modulate(toSfColor, rhs.toSfColor));
	}

	uint toUint() const { return sfColor_toInteger(toSfColor()); }
	static Color fromUint(uint u) { return Color.fromSfColor(sfColor_fromInteger(u)); }
}
