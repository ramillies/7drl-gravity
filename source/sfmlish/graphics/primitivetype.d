module sfmlish.graphics.primitivetype;

import sfmlish.csfml;

enum PrimitiveType { Points, Lines, LineStrip, Triangles, TriangleStrip, TriangleFan, Quads };
