module sfmlish.graphics.convexshape;

import sfmlish.graphics.shape;
import sfmlish.graphics.transformable;
import sfmlish.graphics.drawable;
import sfmlish.graphics.rendertarget;
import sfmlish.graphics.transform;
import sfmlish.graphics.color;
import sfmlish.graphics.texture;
import sfmlish.graphics.renderstates;
import sfmlish.system.vec;
import sfmlish.system.rect;

import sfmlish.callbackmixins;

import sfmlish.csfml;

class ConvexShape: Transformable, Drawable, Bboxable
{
	private sfConvexShape *t;

	this() { t = sfConvexShape_create(); }
	
	mixin(transformationsForType("sfConvexShape"));
	mixin(bboxesForType("sfConvexShape"));
	mixin(shapeMethodsForType("sfConvexShape"));
	mixin(relativeOriginMixin);
	mixin(drawableMixin);

	@property final void pointCount(size_t c) { sfConvexShape_setPointCount(t, c); }
	final void setPoint(size_t index, Vec2 point) { sfConvexShape_setPoint(t, index, point.toVector2f); }
	@property final void points(Vec2[] pts)
	{
		this.pointCount = pts.length;
		foreach(n, p; pts)
			setPoint(n, p);
	}

	@property sfConvexShape *internalPtr() { return t; }

	mixin(propertyCallbacksMixin);
}
