module sfmlish.graphics.shader;

import std.string;
import std.array;
import std.algorithm;
import std.range;
import std.conv;

import sfmlish.csfml;

import sfmlish.system.inputstream;
import sfmlish.system.vec;
import sfmlish.graphics.texture;
import sfmlish.graphics.color;

private struct CurrentTextureParameter { }
enum CurrentTexture = CurrentTextureParameter();

class Shader
{
	private sfShader *t;

	static bool isSupported() { return sfShader_isAvailable().to!bool; }
	static bool isGeometryShaderSupported() { return sfShader_isGeometryAvailable().to!bool; }

	this(sfShader *s) { t = s; }
	~this() { sfShader_destroy(t); }
	static Shader fromFile(string[string] paths)
	{
		return new Shader(sfShader_createFromFile(
			"vertex" in paths ? paths["vertex"].toStringz : null,
			"geometry" in paths ? paths["geometry"].toStringz : null,
			"fragment" in paths ? paths["fragment"].toStringz : null
		));
	}
	static Shader fromMemory(string[string] code)
	{
		return new Shader(sfShader_createFromMemory(
			"vertex" in code ? code["vertex"].toStringz : null,
			"geometry" in code ? code["geometry"].toStringz : null,
			"fragment" in code ? code["fragment"].toStringz : null
		));
	}
	static Shader fromStream(InputStream[string] streams)
	{
		sfInputStream vertex, geometry, fragment;
		if("vertex" in streams)
			vertex = streams["vertex"].toSfInputStream;
		if("geometry" in streams)
			geometry = streams["geometry"].toSfInputStream;
		if("fragment" in streams)
			fragment = streams["fragment"].toSfInputStream;
		return new Shader(sfShader_createFromStream(
			"vertex" in streams ? &vertex : null,
			"geometry" in streams ? &geometry : null,
			"fragment" in streams ? &fragment : null
		));
	}

	void opIndexAssign(Vec2 val, string name) { sfShader_setVec2Uniform(t, name.toStringz, val.toVector2f); }
	void opIndexAssign(Vec3 val, string name) { sfShader_setVec3Uniform(t, name.toStringz, val.toVector3f); }
	void opIndexAssign(Vec4 val, string name) { sfShader_setVec4Uniform(t, name.toStringz, sfGlslVec4(val.x, val.y, val.z, val.w)); }

	void opIndexAssign(Color val, string name) { sfShader_setColorUniform(t, name.toStringz, val.toSfColor); }

	void opIndexAssign(float val, string name) { sfShader_setFloatUniform(t, name.toStringz, val); }
	void opIndexAssign(int val, string name) { sfShader_setIntUniform(t, name.toStringz, val); }
	void opIndexAssign(bool val, string name) { sfShader_setBoolUniform(t, name.toStringz, val.to!uint); }

	void opIndexAssign(int[2] val, string name) { sfShader_setIvec2Uniform(t, name.toStringz, sfGlslIvec2(val[0], val[1])); }
	void opIndexAssign(int[3] val, string name) { sfShader_setIvec3Uniform(t, name.toStringz, sfGlslIvec3(val[0], val[1], val[2])); }
	void opIndexAssign(int[4] val, string name) { sfShader_setIvec4Uniform(t, name.toStringz, sfGlslIvec4(val[0], val[1], val[2], val[3])); }

	void opIndexAssign(bool[2] val, string name) { sfShader_setBvec2Uniform(t, name.toStringz, sfGlslBvec2(val[0].to!uint, val[1].to!uint)); }
	void opIndexAssign(bool[3] val, string name) { sfShader_setBvec3Uniform(t, name.toStringz, sfGlslBvec3(val[0].to!uint, val[1].to!uint, val[2].to!uint)); }
	void opIndexAssign(bool[4] val, string name) { sfShader_setBvec4Uniform(t, name.toStringz, sfGlslBvec4(val[0].to!uint, val[1].to!uint, val[2].to!uint, val[3].to!uint)); }

	void opIndexAssign(float[3][3] val, string name)
	{
		sfGlslMat3 m = sfGlslMat3([
			val[0][0], val[0][1], val[0][2],
			val[1][0], val[1][1], val[1][2],
			val[2][0], val[2][1], val[2][2]]
		);
		sfShader_setMat3Uniform(t, name.toStringz, &m);
	}
	void opIndexAssign(float[4][4] val, string name)
	{
		sfGlslMat4 m = sfGlslMat4([
			val[0][0], val[0][1], val[0][2], val[0][3],
			val[1][0], val[1][1], val[1][2], val[1][3],
			val[2][0], val[2][1], val[2][2], val[2][3],
			val[3][0], val[3][1], val[3][2], val[3][3]
		]);
		sfShader_setMat4Uniform(t, name.toStringz, &m);
	}

	void opIndexAssign(Texture val, string name) { sfShader_setTextureUniform(t, name.toStringz, val.internalPtr); }
	void opIndexAssign(CurrentTextureParameter val, string name) { sfShader_setCurrentTextureUniform(t, name.toStringz); }

	void opIndexAssign(float[] val, string name) { sfShader_setFloatUniformArray(t, name.toStringz, val.ptr, val.length); }
	void opIndexAssign(Vec2[] val, string name)
	{
		sfVector2f[] sfVal = val.map!((v) => v.toVector2f).array;
		sfShader_setVec2UniformArray(t, name.toStringz, sfVal.ptr, sfVal.length);
	}
	void opIndexAssign(Vec3[] val, string name)
	{
		sfVector3f[] sfVal = val.map!((v) => v.toVector3f).array;
		sfShader_setVec3UniformArray(t, name.toStringz, sfVal.ptr, sfVal.length);
	}
	void opIndexAssign(Vec4[] val, string name)
	{
		sfGlslVec4[] sfVal = val.map!((v) => sfGlslVec4(v.x, v.y, v.z, v.w)).array;
		sfShader_setVec4UniformArray(t, name.toStringz, sfVal.ptr, sfVal.length);
	}

	void opIndexAssign(float[3][3][] val, string name)
	{
		sfGlslMat3[] m = val.map!((v) => sfGlslMat3([
			v[0][0], v[0][1], v[0][2],
			v[1][0], v[1][1], v[1][2],
			v[2][0], v[2][1], v[2][2]]
		)).array;
		sfShader_setMat3UniformArray(t, name.toStringz, m.ptr, m.length);
	}
	void opIndexAssign(float[4][4][] val, string name)
	{
		sfGlslMat4[] m = val.map!((v) => sfGlslMat4([
			v[0][0], v[0][1], v[0][2], v[0][3],
			v[1][0], v[1][1], v[1][2], v[1][3],
			v[2][0], v[2][1], v[2][2], v[2][3],
			v[3][0], v[3][1], v[3][2], v[3][3]
		])).array;
		sfShader_setMat4UniformArray(t, name.toStringz, m.ptr, m.length);
	}

	@property uint nativeHandle() { return sfShader_getNativeHandle(t); }
	@property sfShader *internalPtr() { return t; }

	static void bind(Shader s) { sfShader_bind(s is null ? null : s.internalPtr); }
}
