module sfmlish.graphics.rectangleshape;

import sfmlish.graphics.shape;
import sfmlish.graphics.transformable;
import sfmlish.graphics.drawable;
import sfmlish.graphics.rendertarget;
import sfmlish.graphics.transform;
import sfmlish.graphics.color;
import sfmlish.graphics.texture;
import sfmlish.graphics.renderstates;
import sfmlish.system.vec;
import sfmlish.system.rect;
import sfmlish.callbackmixins;

import sfmlish.csfml;

class RectangleShape: Transformable, Drawable, Bboxable
{
	private sfRectangleShape *t;

	this() { t = sfRectangleShape_create(); }
	
	mixin(transformationsForType("sfRectangleShape"));
	mixin(bboxesForType("sfRectangleShape"));
	mixin(shapeMethodsForType("sfRectangleShape"));
	mixin(relativeOriginMixin);
	mixin(drawableMixin);

	@GenerateCallback
	{
		@property Vec2 size() const { return Vec2.fromVector2f(sfRectangleShape_getSize(t)); }
		@property void size(Vec2 v) { sfRectangleShape_setSize(t, v.toVector2f); }
	}

	@property sfRectangleShape *internalPtr() { return t; }

	mixin(propertyCallbacksMixin);
}
