module sfmlish.graphics.vertex;

import sfmlish.csfml;
import sfmlish.system.vec;
import sfmlish.graphics.color;

struct Vertex
{
	Vec2 pos = Vec2(0, 0);
	Color color = Color(0,0,0,255);
	Vec2 texCoords = Vec2(0, 0);

	sfVertex toSfVertex() const { return sfVertex(pos.toVector2f, color.toSfColor, texCoords.toVector2f); }
	static Vertex fromSfVertex(sfVertex v) { return Vertex(Vec2.fromVector2f(v.position), Color.fromSfColor(v.color), Vec2.fromVector2f(v.texCoords)); }
}
