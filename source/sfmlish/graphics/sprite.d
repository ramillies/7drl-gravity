module sfmlish.graphics.sprite;

import sfmlish.csfml;

import sfmlish.graphics.color;
import sfmlish.graphics.transform;
import sfmlish.graphics.transformable;
import sfmlish.graphics.texture;
import sfmlish.graphics.drawable;
import sfmlish.graphics.rendertarget;
import sfmlish.graphics.renderstates;
import sfmlish.system.vec;
import sfmlish.system.rect;
import sfmlish.callbackmixins;
import sfmlish.graphics.textureaccessmixin;

import std.conv;
import std.typecons: Rebindable;

class Sprite: Transformable, Drawable, Bboxable
{
	private Rebindable!(const(Texture)) currentTexture;
	protected sfSprite *t;

	this() { t = sfSprite_create(); }
	this(sfSprite *ptr) { t = ptr; }

	~this() { sfSprite_destroy(t); }

	static Sprite create() { return new Sprite(sfSprite_create()); }

	@GenerateCallback
	{
		@property Color color() const { return Color.fromSfColor(sfSprite_getColor(t)); }
		@property void color(Color c) { sfSprite_setColor(t, c.toSfColor()); }
	}
	@property const(Texture) texture() const { return currentTexture; }
	@property void texture(const Texture tex)
	{
		sfSprite_setTexture(t, (cast(Texture) tex).internalPtr, 0);
		currentTexture = tex;
		useTextureFromStore = false;
	}
	@property Rect textureRect() const { return Rect.fromIntRect(sfSprite_getTextureRect(t)); }
	@property void textureRect(Rect r) { sfSprite_setTextureRect(t, r.toIntRect); }

	Sprite dup() const { return new Sprite(sfSprite_copy(t)); }

	mixin(transformationsForType("sfSprite"));
	mixin(relativeOriginMixin);
	mixin(bboxesForType("sfSprite"));

	@property sfSprite *internalPtr() { return t; }

	mixin(drawableMixin);
	mixin(propertyCallbacksMixin);

	mixin(textureAccessMixin);
}
