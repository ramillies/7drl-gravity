module sfmlish.graphics.rendertexture;

import std.conv;

import sfmlish.csfml;

import sfmlish.system.vec;
import sfmlish.graphics;
import sfmlish.window.videomode;
import sfmlish.window.window;
import sfmlish.window.contextsettings;
import sfmlish.util;

class RenderTexture: RenderTarget
{
	private sfRenderTexture* win;

	this(uint width, uint height, ContextSettings settings = ContextSettings())
	{
		win = sfRenderTexture_createWithSettings(width, height, settings.toSfStruct);
	}

	~this() { sfRenderTexture_destroy(win); }

	@property Vec2 size() const { return Vec2.fromVector2u(sfRenderTexture_getSize(win)); }

	void clear(Color c = Color.Black) { sfRenderTexture_clear(win, c.toSfColor); }
	void display() { sfRenderTexture_display(win); }

	void draw(Sprite what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderTexture_drawSprite(win, what.internalPtr, &s); }
	void draw(Text what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderTexture_drawText(win, what.internalPtr, &s); }
	void draw(const(sfShape) *what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderTexture_drawShape(win, what, &s); }
	void draw(CircleShape what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderTexture_drawCircleShape(win, what.internalPtr, &s); }
	void draw(ConvexShape what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderTexture_drawConvexShape(win, what.internalPtr, &s); }
	void draw(RectangleShape what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderTexture_drawRectangleShape(win, what.internalPtr, &s); }
	void draw(VertexArray what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderTexture_drawVertexArray(win, what.internalPtr, &s); }
	void draw(VertexBuffer what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderTexture_drawVertexBuffer(win, what.internalPtr, &s); }
	void draw(Drawable what, RenderStates states = RenderStates()) { what.draw(this, states); }

	@property void view(View v) { sfRenderTexture_setView(win, v.internalPtr); }
	@property View view() const { return new View(sfView_copy(sfRenderTexture_getView(win))); }
	@property View defaultView() const { return new View(sfView_copy(sfRenderTexture_getDefaultView(win))); }

	Vec2 pixelToCoords(Vec2 point, View v = null) const { return Vec2.fromVector2f(sfRenderTexture_mapPixelToCoords(win, point.toVector2i, v is null ? null : v.internalPtr)); }
	Vec2 coordsToPixel(Vec2 point, View v = null) const { return Vec2.fromVector2i(sfRenderTexture_mapCoordsToPixel(win, point.toVector2f, v is null ? null : v.internalPtr)); }

	@property immutable(Texture) texture() const { return cast(immutable(Texture)) new Texture(sfTexture_copy(cast(sfTexture *) sfRenderTexture_getTexture(win))); }

	@property void smooth(bool b) { sfRenderTexture_setSmooth(win, b.to!uint); }
	@property bool smooth() const { return sfRenderTexture_isSmooth(win).to!bool; }

	@property void repeated(bool b) { sfRenderTexture_setRepeated(win, b.to!uint); }
	@property bool repeated() const { return sfRenderTexture_isRepeated(win).to!bool; }

	@property sfRenderTexture *internalPtr() { return win; }

	void pushGLStates() { sfRenderTexture_pushGLStates(win); }
	void popGLStates() { sfRenderTexture_popGLStates(win); }
	void resetGLStates() { sfRenderTexture_resetGLStates(win); }

	void activate() { sfRenderTexture_setActive(win, 1); }
	void deactivate() { sfRenderTexture_setActive(win, 0); }
}
