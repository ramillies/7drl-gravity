module sfmlish.graphics.transformable;

import std.format;

import sfmlish.csfml;

import sfmlish.system.vec;
import sfmlish.system.rect;
import sfmlish.graphics.transform;


interface Transformable
{
	@property Vec2 pos() const;
	@property float rotation() const;
	@property Vec2 scale() const;
	@property Vec2 origin() const;
	@property void pos(Vec2 v);
	@property void rotation(float f);
	@property void scale(Vec2 v);
	@property void origin(Vec2 v);

	@property Matrix matrix() const;
	@property Matrix inverseMatrix() const;
}

interface Bboxable
{
	@property Rect bounds() const;
	@property Rect localBounds() const;

	@property void relativeOrigin(Vec2 v);
	@property Vec2 relativeOrigin() const;
	void updateOrigin();
	void noRelativeOrigin();
}

string transformationsForType(string type)
{
	return format(`
	@GenerateCallback
	{
		@property Vec2 pos() const { return Vec2.fromVector2f(%1$s_getPosition(t)); }
		@property float rotation() const { return %1$s_getRotation(t); }
		@property Vec2 scale() const { return Vec2.fromVector2f(%1$s_getScale(t)); }
		@property Vec2 origin() const { return Vec2.fromVector2f(%1$s_getOrigin(t)); }
		@property void pos(Vec2 v) { %1$s_setPosition(t, v.toVector2f); }
		@property void rotation(float f) { %1$s_setRotation(t, f); }
		@property void scale(Vec2 v) { %1$s_setScale(t, v.toVector2f); }
		@property void origin(Vec2 v) { %1$s_setOrigin(t, v.toVector2f); }
	}

	@property Matrix matrix() const { return Matrix(%1$s_getTransform(t)); }
	@property Matrix inverseMatrix() const { return Matrix(%1$s_getInverseTransform(t)); }
	`, type);
}

string bboxesForType(string type)
{
	return format(`
	@property Rect bounds() const { return Rect.fromFloatRect(%1$s_getGlobalBounds(t)); }
	@property Rect localBounds() const { return Rect.fromFloatRect(%1$s_getLocalBounds(t)); }
	`, type);
}

enum relativeOriginMixin = `
private Vec2 _relativeOrigin = Vec2(0.5, 0.5);
private bool _relativeOriginAllowed = false;

void noRelativeOrigin() { _relativeOriginAllowed = false; }
void updateOrigin() { if(_relativeOriginAllowed) origin = localBounds().relativePoint(_relativeOrigin); }

@GenerateCallback @property void relativeOrigin(Vec2 v)
{
	_relativeOrigin = v;
	_relativeOriginAllowed = true;
	updateOrigin;
}

@GenerateCallback @property Vec2 relativeOrigin() const { return _relativeOrigin; }
`;

enum customTransformable = `
private Transform transform;

@property Vec2 pos() const { return transform.pos; }
@property float rotation() const { return transform.rotation; }
@property Vec2 scale() const { return transform.scale; }
@property Vec2 origin() const { return transform.origin; }
@property void pos(Vec2 v) { transform.pos = v; }
@property void rotation(float f) { transform.rotation = f; }
@property void scale(Vec2 v) { transform.scale = v; }
@property void origin(Vec2 v) { transform.origin = v; }

@property Matrix matrix() const { return transform.matrix; }
@property Matrix inverseMatrix() const { return transform.inverseMatrix; }
`;

enum transformableBbox = `
	@property Rect bounds() const
	{
		return transform.matrix * localBounds();
	}
`;
