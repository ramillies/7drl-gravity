module sfmlish.graphics.font;


import sfmlish.csfml;

import sfmlish.graphics.glyph;
import sfmlish.system.inputstream;
import sfmlish.graphics.texture;
import sfmlish.system.exception;

import std.conv;
import std.functional;
import std.stdio;
import std.string;

struct FontInfo { string family; }

class Font
{
	private sfFont *f;
	private InputStream streamForHolding;

	this(sfFont *ptr) { f = ptr; }
	~this() { sfFont_destroy(f); }

	static Font fromFile(string path)
	{
		auto result = sfFont_createFromFile(path.toStringz);
		if(result is null) throw new SFMLException("Failed to load font from file '" ~ path ~ "'.");
		return new Font(result);
	}

	static Font fromMemory(const(ubyte)[] bytes)
	{
		auto result = sfFont_createFromMemory(cast(const(void) *) bytes.ptr, bytes.length);
		if(result is null) throw new SFMLException("Failed to load font from memory.");
		return new Font(result);
	}

	static Font fromStream(InputStream stream)
	{
		sfInputStream s = stream.toSfInputStream;
		auto result = sfFont_createFromStream(&s);
		if(result is null) throw new SFMLException("Failed to load font from input stream: " ~ stream.to!string ~ ".");
		auto font = new Font(result);
		font.holdStream(stream);
		return font;
	}

	private void holdStream(InputStream stream) { streamForHolding = stream; }

	Glyph glyph(dchar character, uint fontSize, bool isBold = false, float outline = 0.) const
	{
		return Glyph.fromSfStruct(sfFont_getGlyph(f, character.to!uint, fontSize, isBold, outline));
	}

	@property FontInfo info() const { sfFontInfo i = sfFont_getInfo(f); return FontInfo(i.family.fromStringz.idup); }

	float kerning(dchar left, dchar right, uint fontSize) const { return sfFont_getKerning(f, left.to!uint, right.to!uint, fontSize); }
	float lineSpacing(uint fontSize) const { return sfFont_getLineSpacing(f, fontSize); }
	float underlinePos(uint fontSize) const { return sfFont_getUnderlinePosition(f, fontSize); }
	float underlineThickness(uint fontSize) const { return sfFont_getUnderlineThickness(f, fontSize); }

	const(Texture) texture(uint fontSize) const
	{
		static Texture[sfTexture *] textureTable;
		sfTexture *bareTex = cast(sfTexture *) sfFont_getTexture(cast(sfFont *) f, fontSize);

		if(bareTex in textureTable)
			return cast(const(Texture)) textureTable[bareTex];

		auto tex = new Texture(bareTex);

		// This gets a pointer to the internal texture of the font that is being filled with the glyphs.
		// As such, the SFML's Font class destroys it when the font is destroyed. However, D will garbage-collect
		// the texture as well, resulting in a double-free.
		// Hence we need to tell the GC to leave this pointer alone.
		import core.memory;
		GC.addRoot(cast(void*) tex);

		textureTable[bareTex] = tex;

		return cast(const(Texture)) tex;
	}

	@property sfFont *internalPtr() { return f; }
}
