module sfmlish.graphics.shape;

import std.format;

string shapeMethodsForType(string type)
{
	return format(`
	import std.typecons: Rebindable;
	import sfmlish.graphics.textureaccessmixin;

	private Rebindable!(const(Texture)) tex;

	@GenerateCallback
	{
		@property Color color() { return Color.fromSfColor(%1$s_getFillColor(t)); }
		@property void color(Color c) { %1$s_setFillColor(t, c.toSfColor); }
		@property Color outlineColor() { return Color.fromSfColor(%1$s_getOutlineColor(t)); }
		@property void outlineColor(Color c) { %1$s_setOutlineColor(t, c.toSfColor); }
		@property float outlineThickness() { return %1$s_getOutlineThickness(t); }
		@property void outlineThickness(float f) { %1$s_setOutlineThickness(t, f); }
	}
	@property void texture(const(Texture) f)
	{
		%1$s_setTexture(t, (cast(Texture) f).internalPtr, 0);
		tex = f;
		useTextureFromStore = false;
	}
	@property const(Texture) texture() { return tex; }
	@property Rect textureRect() { return Rect.fromIntRect(%1$s_getTextureRect(t)); }
	@property void textureRect(Rect r) { %1$s_setTextureRect(t, r.toIntRect); }

	@property size_t pointCount() { return %1$s_getPointCount(t); }
	Vec2 point(size_t index) { return Vec2.fromVector2f(%1$s_getPoint(t, index)); }

	mixin(textureAccessMixin);
	`, type);
}
