module sfmlish.graphics.texture;

import std.conv;
import std.stdio;
import std.string;

import sfmlish.system;
import sfmlish.graphics.image;
import sfmlish.graphics.renderwindow;
import sfmlish.window.window;
import sfmlish.system.exception;

import sfmlish.csfml;

class Texture
{
	private sfTexture *t;

	this(sfTexture *ptr) { t = ptr; }
	~this() { sfTexture_destroy(t); }

	static Texture create(uint width, uint height)
	{
		auto result = sfTexture_create(width, height);
		if(result is null) throw new SFMLException("Failed to make empty texture of dimensions " ~ width.to!string ~ " × " ~ height.to!string);
		return new Texture(result);
	}
	static Texture fromFile(string path, Rect rect = Rect(0,0,0,0))
	{
		sfIntRect r = rect.toIntRect;
		sfTexture *result = sfTexture_createFromFile(path.toStringz, &r);
		if(result is null) throw new SFMLException(`Failed to load texture from file '` ~ path ~ `'.`);
		return new Texture(result);
	}
	static Texture fromMemory(const(ubyte)[] bytes, Rect rect = Rect(0,0,0,0))
	{
		sfIntRect r = rect.toIntRect;
		sfTexture *result = sfTexture_createFromMemory(cast(const(void) *) bytes.ptr, bytes.length, &r);
		if(result is null) throw new SFMLException(`Failed to load texture from memory.`);
		return new Texture(result);
	}
	static Texture fromStream(InputStream stream, Rect rect = Rect(0,0,0,0))
	{
		sfIntRect r = rect.toIntRect;
		sfInputStream s = stream.toSfInputStream;
		sfTexture *result = sfTexture_createFromStream(&s, &r);
		if(result is null) throw new SFMLException(`Failed to load texture from input stream: ` ~ stream.to!string ~ `.`);
		return new Texture(result);
	}
	static Texture fromImage(Image image, Rect rect = Rect(0,0,0,0))
	{
		sfIntRect r = rect.toIntRect;
		sfTexture *result = sfTexture_createFromImage(image.internalPtr, &r);
		if(result is null) throw new SFMLException(`Failed to load texture from an image.`);
		return new Texture(result);
	}

	@property void smooth(bool b) { sfTexture_setSmooth(t, b.to!uint); }
	@property bool smooth() const { return sfTexture_isSmooth(t).to!bool; }
	@property void repeated(bool b) { sfTexture_setRepeated(t, b.to!uint); }
	@property bool repeated() const { return sfTexture_isRepeated(t).to!bool; }
	@property void srgb(bool b) { sfTexture_setSrgb(t, b.to!uint); }
	@property bool srgb() const { return sfTexture_isSrgb(t).to!bool; }

	bool makeMipmaps() { return sfTexture_generateMipmap(t).to!bool; }

	@property Vec2 size() const { return Vec2.fromVector2u(sfTexture_getSize(t)); }

	Texture dup() const { return new Texture(sfTexture_copy(t)); }

	@property sfTexture *internalPtr() { return t; }

	Image toImage() const { return new Image(sfTexture_copyToImage(t)); }
	@property uint nativeHandle() const { return sfTexture_getNativeHandle(t); }

	mixin(updateMixin("Texture"));
	mixin(updateMixin("Image"));
	mixin(updateMixin("Window"));
	mixin(updateMixin("RenderWindow"));

	void update(const(ubyte)[] pixels, Vec2 size, Vec2 pos = Vec2(0,0))
	{
		sfTexture_updateFromPixels(t, pixels.ptr, size.x.to!uint, size.y.to!uint, pos.x.to!uint, pos.y.to!uint);
	}

	static void bind(Texture t) { sfTexture_bind(t is null ? null : t.internalPtr); }
}

private string updateMixin(string type)
{
	return format(`
		void update(%1$s other, Vec2 pos = Vec2(0,0))
		{
			sfTexture_updateFrom%1$s(t, other.internalPtr, pos.x.to!uint, pos.y.to!uint);
		}
	`, type);
}
