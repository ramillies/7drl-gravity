module sfmlish.graphics.view;

import sfmlish.system.rect;
import sfmlish.system.vec;
import sfmlish.graphics.transform;

import sfmlish.csfml;

class View
{
	private sfView *t;

	this() { t = sfView_create(); }
	this(sfView *ptr) { t = ptr; }
	this(Rect r) { t = sfView_createFromRect(r.toFloatRect); }
	~this() { sfView_destroy(t); }

	@property Vec2 size() const { return Vec2.fromVector2f(sfView_getSize(t)); }
	@property Vec2 center() const { return Vec2.fromVector2f(sfView_getCenter(t)); }
	@property Rect rectangle() const { return Rect(center.x-size.x/2, center.y-size.y/2, size.x, size.y); }
	@property float rotation() const { return sfView_getRotation(t); }
	@property Rect viewport() const { return Rect.fromFloatRect(sfView_getViewport(t)); }

	@property void size(Vec2 v) { sfView_setSize(t, v.toVector2f); }
	@property void center(Vec2 v) { sfView_setCenter(t, v.toVector2f); }
	@property void rectangle(Rect r) { size = Vec2(r.width, r.height); center = r.relativePoint(Vec2(.5, .5)); }
	@property void rotation(float r) { sfView_setRotation(t, r); }
	@property void viewport(Rect r) { sfView_setViewport(t, r.toFloatRect); }

	void reset(Rect r) { sfView_reset(t, r.toFloatRect); }
	View dup() const { return new View(sfView_copy(t)); }

	@property sfView *internalPtr() { return t; }
}
