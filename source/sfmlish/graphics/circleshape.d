module sfmlish.graphics.circleshape;

import sfmlish.graphics.shape;
import sfmlish.graphics.transformable;
import sfmlish.graphics.transform;
import sfmlish.graphics.drawable;
import sfmlish.graphics.rendertarget;
import sfmlish.graphics.color;
import sfmlish.graphics.texture;
import sfmlish.graphics.renderstates;
import sfmlish.system.vec;
import sfmlish.system.rect;
import sfmlish.callbackmixins;

import sfmlish.csfml;

class CircleShape: Transformable, Drawable, Bboxable
{
	private sfCircleShape *t;

	this() { t = sfCircleShape_create(); }
	
	mixin(transformationsForType("sfCircleShape"));
	mixin(bboxesForType("sfCircleShape"));
	mixin(shapeMethodsForType("sfCircleShape"));
	mixin(relativeOriginMixin);
	mixin(drawableMixin);

	@GenerateCallback
	{
		@property float radius() const { return sfCircleShape_getRadius(t); }
		@property void radius(float f) { sfCircleShape_setRadius(t, f); }
	}
	@property void pointCount(size_t c) { sfCircleShape_setPointCount(t, c); }

	@property sfCircleShape *internalPtr() { return t; }

	mixin(propertyCallbacksMixin);
}
