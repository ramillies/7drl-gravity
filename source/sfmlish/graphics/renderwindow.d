module sfmlish.graphics.renderwindow;

import std.conv;

import sfmlish.csfml;

import sfmlish.system.vec;
import sfmlish.system.rect;
import sfmlish.graphics;
import sfmlish.window.contextsettings;
import sfmlish.window.event;
import sfmlish.window.cursor;
import sfmlish.window.videomode;
import sfmlish.window.window;
import sfmlish.util;

class RenderWindow: RenderTarget
{
	mixin(windowMethodsForType("RenderWindow"));

	void clear(Color c = Color.Black) { sfRenderWindow_clear(win, c.toSfColor); }

	void draw(Sprite what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderWindow_drawSprite(win, what.internalPtr, &s); }
	void draw(Text what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderWindow_drawText(win, what.internalPtr, &s); }
	void draw(const(sfShape) *what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderWindow_drawShape(win, what, &s); }
	void draw(CircleShape what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderWindow_drawCircleShape(win, what.internalPtr, &s); }
	void draw(ConvexShape what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderWindow_drawConvexShape(win, what.internalPtr, &s); }
	void draw(RectangleShape what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderWindow_drawRectangleShape(win, what.internalPtr, &s); }
	void draw(VertexArray what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderWindow_drawVertexArray(win, what.internalPtr, &s); }
	void draw(VertexBuffer what, RenderStates states = RenderStates()) { sfRenderStates s = states.toSfRenderStates; sfRenderWindow_drawVertexBuffer(win, what.internalPtr, &s); }
	void draw(Drawable what, RenderStates states = RenderStates()) { what.draw(this, states); }

	@property void view(View v) { sfRenderWindow_setView(win, v.internalPtr); }
	@property View view() const { return new View(sfView_copy(sfRenderWindow_getView(win))); }
	@property View defaultView() const { return new View(sfView_copy(sfRenderWindow_getDefaultView(win))); }
	@property Rect viewport(View v) const { return Rect.fromIntRect(sfRenderWindow_getViewport(win, v.internalPtr)); }

	Vec2 pixelToCoords(Vec2 point, View v = null) const { return Vec2.fromVector2f(sfRenderWindow_mapPixelToCoords(win, point.toVector2i, v is null ? null : v.internalPtr)); }
	Vec2 coordsToPixel(Vec2 point, View v = null) const { return Vec2.fromVector2i(sfRenderWindow_mapCoordsToPixel(win, point.toVector2f, v is null ? null : v.internalPtr)); }

	Image screenshot() const { return new Image(sfRenderWindow_capture(win)); }

	void pushGLStates() { sfRenderWindow_pushGLStates(win); }
	void popGLStates() { sfRenderWindow_popGLStates(win); }
	void resetGLStates() { sfRenderWindow_resetGLStates(win); }
}
