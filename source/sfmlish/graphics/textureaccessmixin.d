module sfmlish.graphics.textureaccessmixin;

enum textureAccessMixin = `
	import sfmlish.game.resources;
	import sfmlish.system.exception;
	import sfmlish.system.rect;

	import std.conv;
	private
	{
		bool useTextureFromStore = false;
		string textureName;
		TextureInfo textureInfo;
		int tilenum;
	}

	@GenerateCallback
	{
		@property void texture(string texName)
		{
			textureName = texName;
			texture = Textures.get(texName);
			textureInfo = Textures.info(texName);
			useTextureFromStore = true;
			tilenumber = 0;
		}
		@property int tilenumber() const
		{
			if(!useTextureFromStore)
				throw new SFMLException("Cannot get tile number for a sprite that hasn't been loaded with a texture from Textures.");
			return tilenum;
		}
		@property void tilenumber(int n)
		{
			if(!useTextureFromStore)
				throw new SFMLException("Cannot set tile number for a sprite that hasn't been loaded with a texture from Textures.");
			auto tilecount = textureInfo.tileCount;
			tilenum = (n%tilecount + tilecount) % tilecount;

			textureRect = Textures.tileForTexture(textureName, n);
		}
	}

	@property void tilename(string x)
	{
		if(!useTextureFromStore)
			throw new SFMLException("Cannot set tile number for a sprite that hasn't been loaded with a texture from Textures.");
		textureRect = Textures.tileForTexture(textureName, x);
	}
`;
