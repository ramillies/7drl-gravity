module sfmlish.graphics.vertexarray;

import sfmlish.csfml;
import sfmlish.graphics.vertex;
import sfmlish.graphics.primitivetype;
import sfmlish.graphics.rendertarget;
import sfmlish.graphics.renderstates;
import sfmlish.graphics.drawable;
import sfmlish.system.rect;

class VertexArray: Drawable
{
	private sfVertexArray *t;

	this() { t = sfVertexArray_create(); }
	~this() { sfVertexArray_destroy(t); }

	void append(Vertex v) { sfVertexArray_append(t, v.toSfVertex); }
	void append(Vertex[] vertices) { foreach(v; vertices) append(v); }
	void clear() { sfVertexArray_clear(t); }
	
	@property PrimitiveType primitiveType() { return cast(PrimitiveType) sfVertexArray_getPrimitiveType(t); }
	@property void primitiveType(PrimitiveType p) { sfVertexArray_setPrimitiveType(t, cast(sfPrimitiveType) p); }

	@property Rect bounds() { return Rect.fromFloatRect(sfVertexArray_getBounds(t)); }
	@property size_t vertexCount() { return sfVertexArray_getVertexCount(t); }

	mixin(drawableMixin);

	@property sfVertexArray *internalPtr() { return t; }
}
