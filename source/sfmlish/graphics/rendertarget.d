module sfmlish.graphics.rendertarget;

import sfmlish.csfml;

import sfmlish.system.vec;
import sfmlish.graphics;

interface RenderTarget
{
	@property Vec2 size();

	void clear(Color c);
	void display();

	void draw(Sprite what, RenderStates states);
	void draw(Text what, RenderStates states);
	void draw(const(sfShape) *what, RenderStates states);
	void draw(CircleShape what, RenderStates states);
	void draw(ConvexShape what, RenderStates states);
	void draw(RectangleShape what, RenderStates states);
	void draw(VertexArray what, RenderStates states);
	void draw(VertexBuffer what, RenderStates states);
	void draw(Drawable what, RenderStates states);

	@property void view(View);
	@property View view() const;
	@property View defaultView() const;

	Vec2 pixelToCoords(Vec2 point, View) const;
	Vec2 coordsToPixel(Vec2 point, View) const;

	void activate();
	void deactivate();

	void pushGLStates();
	void popGLStates();
	void resetGLStates();
}
