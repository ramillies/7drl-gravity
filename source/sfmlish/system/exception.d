module sfmlish.system.exception;

import std.exception;

class SFMLException: Exception
{
	mixin basicExceptionCtors;
}
