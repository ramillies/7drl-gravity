module sfmlish.system.vec;

import std.conv;
import std.math;
import sfmlish.csfml;

struct Vec2
{
	double x = 0, y = 0;

	sfVector2f toVector2f() const { return sfVector2f(x, y); }
	sfVector2i toVector2i() const { return sfVector2i(x.to!int, y.to!int); }
	sfVector2u toVector2u() const { return sfVector2u(x.to!uint, y.to!uint); }

	static Vec2 fromVector2f(sfVector2f v) { return Vec2(v.x, v.y); }
	static Vec2 fromVector2i(sfVector2i v) { return Vec2(v.x*1., v.y*1.); }
	static Vec2 fromVector2u(sfVector2u v) { return Vec2(v.x*1., v.y*1.); }

	double dot(Vec2 rhs) const { return x*rhs.x + y*rhs.y; }
	double abs() const { return sqrt(this.dot(this)); }
	double angle() const { return atan2(y, x) * 180. / PI; }
	Vec2 normalized() const { return this/abs(); }

	Vec2 perp() const { return Vec2(-y, x); }
	Vec2 rotated(double degrees) const { return cos(degrees*PI/180) * this + sin(degrees*PI/180) * perp(); }

	Vec2 opUnary(string op)() const
	if(op == "-")
	{
		return Vec2(-x, -y);
	}

	Vec2 opBinary(string op)(Vec2 rhs) const
	if((op == "+") || (op == "-") || (op == "*") || (op == "/"))
	{
		mixin("return Vec2(x " ~ op ~ " rhs.x, y " ~ op ~ " rhs.y);");
	}

	Vec2 opBinary(string op)(double f) const
	if((op == "*") || (op == "/"))
	{
		mixin("return Vec2(x " ~ op ~ " f, y " ~ op ~ " f);");
	}

	Vec2 opBinaryRight(string op)(double f) const
	if(op == "*")
	{
		return Vec2(x*f, y*f);
	}

	ref Vec2 opOpAssign(string op)(Vec2 rhs)
	if((op == "+") || (op == "-") || (op == "*") || (op == "/"))
	{
		mixin("x " ~ op ~ "= rhs.x; y " ~ op ~ "= rhs.y;");
		return this;
	}
}

struct Vec3
{
	double x = 0, y = 0, z = 0;

	sfVector3f toVector3f() const { return sfVector3f(x, y, z); }
	static Vec3 fromVector3f(sfVector3f v) { return Vec3(v.x, v.y, v.z); }

	double dot(Vec3 rhs) const { return x*rhs.x + y*rhs.y + z*rhs.z; }
	double abs() const { return sqrt(this.dot(this)); }
	Vec3 normalized() const { return this/abs(); }
	Vec3 cross(Vec3 rhs) const { return Vec3(y*rhs.z - z*rhs.y, z*rhs.x - x*rhs.z, x*rhs.y - y*rhs.x); }

	Vec3 opUnary(string op)() const
	if(op == "-")
	{
		return Vec3(-x, -y, -z);
	}

	Vec3 opBinary(string op)(Vec3 rhs) const
	if((op == "+") || (op == "-") || (op == "*") || (op == "/"))
	{
		mixin("return Vec3(x " ~ op ~ " rhs.x, y " ~ op ~ " rhs.y, z " ~ op ~ " rhs.z);");
	}

	Vec3 opBinary(string op)(double f) const
	if((op == "*") || (op == "/"))
	{
		mixin("return Vec3(x " ~ op ~ " f, y " ~ op ~ " f, z " ~ op ~ " f);");
	}

	Vec3 opBinaryRight(string op)(double f) const
	if(op == "*")
	{
		return Vec3(x*f, y*f, z*f);
	}

	ref Vec3 opOpAssign(string op)(Vec3 rhs)
	if((op == "+") || (op == "-") || (op == "*") || (op == "/"))
	{
		mixin("x " ~ op ~ "= rhs.x; y " ~ op ~ "= rhs.y; z " ~ op ~ "= rhs.z;");
		return this;
	}
}

struct Vec4
{
	double x = 0, y = 0, z = 0, w = 0;

	double dot(Vec4 rhs) const { return x*rhs.x + y*rhs.y + z*rhs.z + w*rhs.w; }
	double abs() const { return sqrt(this.dot(this)); }
	Vec4 normalized() const { return this/abs(); }

	Vec4 opUnary(string op)() const
	if(op == "-")
	{
		return Vec4(-x, -y, -z, -w);
	}

	Vec4 opBinary(string op)(Vec4 rhs) const
	if((op == "+") || (op == "-") || (op == "*") || (op == "/"))
	{
		mixin("return Vec4(x " ~ op ~ " rhs.x, y " ~ op ~ " rhs.y, z " ~ op ~ " rhs.z, w " ~ op ~ " rhs.w);");
	}

	Vec4 opBinary(string op)(double f) const
	if((op == "*") || (op == "/"))
	{
		mixin("return Vec4(x " ~ op ~ " f, y " ~ op ~ " f, z " ~ op ~ " f, w " ~ op ~ " f);");
	}

	Vec4 opBinaryRight(string op)(double f) const
	if(op == "*")
	{
		return Vec4(x*f, y*f, z*f, w*f);
	}

	ref Vec4 opOpAssign(string op)(Vec4 rhs)
	if((op == "+") || (op == "-") || (op == "*") || (op == "/"))
	{
		mixin("x " ~ op ~ "= rhs.x; y " ~ op ~ "= rhs.y; z " ~ op ~ "= rhs.z; w " ~ op ~ "= rhs.w;");
		return this;
	}
}
