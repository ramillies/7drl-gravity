module sfmlish.system;

public import sfmlish.system.rect;
public import sfmlish.system.vec;
public import sfmlish.system.clock;
public import sfmlish.system.inputstream;
