module sfmlish.system.inputstream;

import sfmlish.csfml;
import sfmlish.system.exception;

import std.stdio;
import std.algorithm: min;
import std.conv;
import std.format;
import std.typecons;

interface InputStream
{
	ubyte[] read(long bytes);
	long seek(long pos);
	long size();
	long tell() const;

	final sfInputStream toSfInputStream()
	{
		extern(C) long readFunc(void *data, long bytes, void *userptr) nothrow
		{
			try
			{
				ubyte[] readBytes = (cast(InputStream) userptr).read(bytes);
				if(readBytes == null)
					return -1;
				foreach(n, c; readBytes)
					*((cast(ubyte *) data) + n) = c;
				return cast(long) readBytes.length;
			}
			catch(Exception e) { return -1; }
		}
		extern(C) long seekFunc(long pos, void *userptr) nothrow
		{
			try { return (cast(InputStream) userptr).seek(pos); }
			catch(Exception e) { return -1; }
		}
		extern(C) long tellFunc(void *userptr) nothrow
		{
			try { return (cast(InputStream) userptr).tell; }
			catch(Exception e) { return -1; }
		}
		extern(C) long sizeFunc(void *userptr) nothrow
		{
			try { return (cast(InputStream) userptr).size; }
			catch(Exception e) { return -1; }
		}

		return sfInputStream(&readFunc, &seekFunc, &tellFunc, &sizeFunc, cast(void *) this);
	}

	final ubyte[] slurp()
	{
		seek(0);
		return read(size());
	}

	final string readUTF8(long bytes) { return cast(string) read(bytes); }
	final string slurpUTF8() { return cast(string) slurp(); }
}

class FileInputStream: InputStream
{
	private string filename;
	private File handle;

	this(string path)
	{
		filename = path;
		handle = File(filename);
	}

	override ubyte[] read(long bytes) { return handle.rawRead(new ubyte[bytes]); }
	override long seek(long pos) { handle.seek(pos); return tell(); }
	override long tell() const { return handle.tell.to!long; }
	override long size() { return handle.size.to!long; }
	override string toString() const { return `FileInputStream('` ~ filename ~`')`; }
}

class MemoryInputStream: InputStream
{
	private Rebindable!(const ubyte[]) buffer;
	private long head;

	this(const ubyte[] data)
	{
		buffer = data;
		head = 0;
	}

	override ubyte[] read(long bytes)
	{
		if(bytes < 0 || size == 0)
			throw new SFMLException(format(`Invalid read of %s bytes at pos %s in memory stream of %s bytes.`, bytes, head, size()));
		long start = head;
		long end = min(head + bytes, size());
		head = end;
		return buffer[start .. end].dup;
	}

	override long seek(long pos)
	{
		if(pos < 0 || buffer.length == 0)
			throw new SFMLException(format(`Invalid seek to pos %s in memory stream of %s bytes.`, pos, size()));
		head = min(pos, size-1);
		return tell();
	}

	override long tell() const
	{
		if(buffer.length == 0)
			throw new SFMLException("Cannot tell() in zero-byte memory stream.");
		return head;
	}

	override long size() { return buffer.length; }
}

unittest
{
	import std.string, std.exception: assertThrown;
	auto stream = new MemoryInputStream("This is a string".representation);
	assert(stream.size == 16);
	assert(stream.tell == 0);
	assert(stream.read(5) == "This ".representation);
	assert(stream.tell == 5);
	stream.seek(10);
	assert(stream.read(10) == "string".representation);
	assert(stream.seek(0) == 0);
	assert(stream.read(7) == "This is".representation);
	stream.seek(42);
	assert(stream.tell == stream.size-1);
}
