module sfmlish.system.rect;

import std.algorithm;
import std.conv;

import sfmlish.csfml;

import sfmlish.system.vec;

struct Rect
{
	float left = 0, top = 0, width = 0, height = 0;

	@property float right() const { return left + width; }
	@property float bottom() const { return top + height; }

	Vec2 relativePoint(Vec2 rel) const { return Vec2(left + rel.x * width, top + rel.y * height); }
	@property Vec2 center() const { return relativePoint(Vec2(.5, .5)); }
	@property Vec2 size() const { return Vec2(width, height); }

	sfFloatRect toFloatRect() const { return sfFloatRect(left, top, width, height); }
	sfIntRect toIntRect() const { return sfIntRect(left.to!int, top.to!int, width.to!int, height.to!int); }

	static Rect fromFloatRect(sfFloatRect r) { return Rect(r.left, r.top, r.width, r.height); }
	static Rect fromIntRect(sfIntRect r) { return Rect(r.left*1., r.top*1., r.width*1., r.height*1.); }

	bool contains(Vec2 v) const
	{
		return (v.x >= left) && (v.x <= right) && (v.y >= top) && (v.y <= bottom);
	}

	bool intersects(Rect r) const
	{
		return (max(left, r.left) <= min(right, r.right)) && (max(top, r.top) <= min(bottom, r.bottom));
	}
}
