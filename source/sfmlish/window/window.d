module sfmlish.window.window;

import std.format;
import std.conv;

import sfmlish.csfml;
import sfmlish.window.videomode;
import sfmlish.window.contextsettings;
import sfmlish.window.event;
import sfmlish.window.cursor;
import sfmlish.system.vec;
import sfmlish.util;

enum WindowStyle: uint { None = 0U, Titlebar = 1U, Resize = 2U, Close = 4U, Fullscreen = 8U, Default = 7U };

string windowMethodsForType(string type)
{
	return format(`
	private sf%1$s* win;

	this(VideoMode mode, string title, WindowStyle style = WindowStyle.Default, ContextSettings settings = ContextSettings())
	{
		sfContextSettings s = settings.toSfStruct;
		win = sf%1$s_createUnicode(mode.toSfMode, stringToUintArray(title).ptr, style, &s);
	}

	~this() { sf%1$s_destroy(win); }

	void close() { sf%1$s_close(win); }
	bool isOpen() const { return sf%1$s_isOpen(win).to!bool; }

	ContextSettings settings() const { return ContextSettings.fromSfStruct(sf%1$s_getSettings(win)); }

	bool pollEvent(out Event e) { sfEvent s; bool retval = sf%1$s_pollEvent(win, &s).to!bool; e = convertSfEvent(s); return retval; }
	bool waitEvent(out Event e) { sfEvent s; bool retval = sf%1$s_waitEvent(win, &s).to!bool; e = convertSfEvent(s); return retval; }

	@property Vec2 pos() const { return Vec2.fromVector2i(sf%1$s_getPosition(win)); }
	@property void pos(Vec2 pos) { sf%1$s_setPosition(win, pos.toVector2i); }
	@property Vec2 size() const { return Vec2.fromVector2u(sf%1$s_getSize(win)); }
	@property void size(Vec2 pos) { sf%1$s_setSize(win, pos.toVector2u); }

	@property void title(string s) { sf%1$s_setUnicodeTitle(win, stringToUintArray(s).ptr); }
	void setIcon(const(ubyte)[] pixels, uint w, uint h) { sf%1$s_setIcon(win, w, h, pixels.ptr); }

	@property void fpsLimit(uint limit) { sf%1$s_setFramerateLimit(win, limit); }
	@property void vsync(bool enabled) { sf%1$s_setVerticalSyncEnabled(win, enabled ? 1 : 0); }
	void show() { sf%1$s_setVisible(win, 1); }
	void hide() { sf%1$s_setVisible(win, 0); }

	@property void cursorVisible(bool b) { sf%1$s_setMouseCursorVisible(win, b.to!uint); }
	@property void cursorGrabbed(bool b) { sf%1$s_setMouseCursorGrabbed(win, b.to!uint); }
	@property void cursor(Cursor c) { sf%1$s_setMouseCursor(win, c.internalPtr); }

	@property void keyRepeat(bool b) { sf%1$s_setKeyRepeatEnabled(win, b.to!uint); }

	void activate() { sf%1$s_setActive(win, 1); }
	void deactivate() { sf%1$s_setActive(win, 0); }

	void requestFocus() { sf%1$s_requestFocus(win); }
	@property bool focus() const { return sf%1$s_hasFocus(win).to!bool; }

	@property void joystickThreshold(float threshold) { sf%1$s_setJoystickThreshold(win, threshold); }

	void display() { sf%1$s_display(win); }

	@property sf%1$s *internalPtr() { return win; }
	`, type);
}

class Window
{
	mixin(windowMethodsForType("Window"));
}
