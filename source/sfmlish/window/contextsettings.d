module sfmlish.window.contextsettings;

import sfmlish.csfml;

enum ContextAttribute {
	Default = 0,
	Core	= 1 << 0,
	Debug   = 1 << 2,
}

struct ContextSettings {
	uint depthBits = 0;
	uint stencilBits = 0;
	uint antialiasingLevel = 0;
	uint majorVersion = 1;
	uint minorVersion = 1;
	uint attributeFlags = ContextAttribute.Default;
	bool sRgbCapable = false;

	sfContextSettings toSfStruct() const
	{
		return sfContextSettings(depthBits, stencilBits, antialiasingLevel, majorVersion, minorVersion, attributeFlags, sRgbCapable ? 1 : 0);
	}

	static ContextSettings fromSfStruct(sfContextSettings s)
	{
		return ContextSettings(s.depthBits, s.stencilBits, s.antialiasingLevel, s.majorVersion, s.minorVersion, s.attributeFlags, s.sRgbCapable == 1);
	}
}
