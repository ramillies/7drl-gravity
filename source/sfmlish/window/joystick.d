module sfmlish.window.joystick;

import std.conv;
import std.string;
import core.stdc.stdlib: free;

import sfmlish.csfml;

enum MaxJoystickCount = sfJoystickCount;
enum MaxJoystickButtonCount = sfJoystickButtonCount;
enum MaxJoystickAxisCount = sfJoystickAxisCount;

enum JoystickAxis { X, Y, Z, R, U, V, PovX, PovY }

struct JoystickInfo
{
	string name;
	uint vendorID, productID;

	static JoystickInfo fromSfStruct(sfJoystickIdentification s)
	{
		auto retval = JoystickInfo(s.name.fromStringz.dup, s.vendorId, s.productId);
		free(cast(void *) s.name);
		return retval;
	}
}

class Joystick
{
	static bool connected(uint joystick) { return sfJoystick_isConnected(joystick).to!bool; }
	static uint buttonCount(uint joystick) { return sfJoystick_getButtonCount(joystick); }
	static bool hasAxis(uint joystick, JoystickAxis axis) { return sfJoystick_hasAxis(joystick, cast(sfJoystickAxis) axis).to!bool; }
	static bool buttonPressed(uint joystick, uint button) { return sfJoystick_isButtonPressed(joystick, button).to!bool; }
	static float axisPos(uint joystick, JoystickAxis axis) { return sfJoystick_getAxisPosition(joystick, cast(sfJoystickAxis) axis); }
	static void update() { sfJoystick_update(); }
	static JoystickInfo info(uint joystick) { return JoystickInfo.fromSfStruct(sfJoystick_getIdentification(joystick)); }
}
