module sfmlish.window.mouse;

import std.conv;

import sfmlish.csfml;
import sfmlish.system.vec;
import sfmlish.window.window;
import sfmlish.graphics.renderwindow;

enum MouseButton { Left, Right, Middle, Extra1, Extra2, ButtonCount }
enum MouseWheel { Vertical, Horizontal }

class Mouse
{
	static Vec2 position(Window win) { return Vec2.fromVector2i(sfMouse_getPosition(win.internalPtr)); }
	static void setPosition(Vec2 pos, Window win) { sfMouse_setPosition(pos.toVector2i, win.internalPtr); }
	static Vec2 position(RenderWindow win) { return Vec2.fromVector2i(sfMouse_getPositionRenderWindow(win.internalPtr)); }
	static void setPosition(Vec2 pos, RenderWindow win) { sfMouse_setPositionRenderWindow(pos.toVector2i, win.internalPtr); }
	static bool buttonPressed(MouseButton b) { return sfMouse_isButtonPressed(cast(sfMouseButton) b).to!bool; }
}
