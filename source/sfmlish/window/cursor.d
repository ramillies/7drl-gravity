module sfmlish.window.cursor;

import sfmlish.csfml;
import sfmlish.system.vec;

enum CursorType {
	Arrow,
	ArrowWait,
	Wait,
	Text,
	Hand,
	SizeHorizontal,
	SizeVertical,
	SizeTopLeftBottomRight,
	SizeBottomLeftTopRight,
	SizeAll,
	Cross,
	Help,
	NotAllowed
}

class Cursor
{
	protected sfCursor *t;

	this(const(ubyte) [] pixels, Vec2 size, Vec2 hotspot)
	{
		t = sfCursor_createFromPixels(pixels.ptr, size.toVector2u, hotspot.toVector2u);
	}

	this(CursorType type)
	{
		t = sfCursor_createFromSystem(cast(sfCursorType) type);
	}

	~this() { sfCursor_destroy(t); }

	sfCursor *internalPtr() { return t; }
}
