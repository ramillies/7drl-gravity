module sfmlish.window.event;

import sfmlish.csfml;
import sfmlish.system.rect;
import sfmlish.system.vec;
import sfmlish.window.keyboard;
import sfmlish.window.mouse;
import sfmlish.window.joystick;

import std.sumtype;
import std.conv;

struct OnClose { }
struct OnResize { Rect viewport; }
struct OnLoseFocus { }
struct OnGainFocus { }
struct OnTextEnter { dchar symbol; }
struct OnKeyPress { Key key; bool alt, ctrl, shift, sys; }
struct OnKeyRelease { Key key; bool alt, ctrl, shift, sys; }
struct OnMouseWheel { MouseWheel wheel; float delta; Vec2 pos; }
struct OnMousePress { MouseButton button; Vec2 pos; }
struct OnMouseRelease { MouseButton button; Vec2 pos; }
struct OnMouseMove { Vec2 pos; }
struct OnMouseEnter { }
struct OnMouseLeave { }
struct OnJoystickPress { uint joystick, button; }
struct OnJoystickRelease { uint joystick, button; }
struct OnJoystickMove { uint joystick; JoystickAxis axis; float pos; }
struct OnJoystickConnect { uint joystick; }
struct OnJoystickDisconnect { uint joystick; }
struct OnTouchPress { uint finger; Vec2 pos; }
struct OnTouchMove { uint finger; Vec2 pos; }
struct OnTouchRelease { uint finger; Vec2 pos; }
struct InvalidEvent { }

alias Event = SumType!(
	OnClose,
	OnResize,
	OnLoseFocus,
	OnGainFocus,
	OnTextEnter,
	OnKeyPress,
	OnKeyRelease,
	OnMouseWheel,
	OnMousePress,
	OnMouseRelease,
	OnMouseMove,
	OnMouseEnter,
	OnMouseLeave,
	OnJoystickPress,
	OnJoystickRelease,
	OnJoystickMove,
	OnJoystickConnect,
	OnJoystickDisconnect,
	OnTouchPress,
	OnTouchMove,
	OnTouchRelease,
	InvalidEvent
);

Event convertSfEvent(sfEvent e)
{
	Event retval;
	if(e.type == sfEvtClosed) retval = OnClose();
	else if(e.type == sfEvtResized) retval = OnResize(Rect(0, 0, e.size.width, e.size.height));
	else if(e.type == sfEvtLostFocus) retval = OnLoseFocus();
	else if(e.type == sfEvtGainedFocus) retval = OnGainFocus();
	else if(e.type == sfEvtTextEntered) retval = OnTextEnter(e.text.unicode.to!dchar);
	else if(e.type == sfEvtKeyPressed) retval = OnKeyPress(cast(Key) e.key.code, e.key.alt == 1, e.key.control == 1, e.key.shift == 1, e.key.system == 1);
	else if(e.type == sfEvtKeyReleased) retval = OnKeyRelease(cast(Key) e.key.code, e.key.alt == 1, e.key.control == 1, e.key.shift == 1, e.key.system == 1);
	else if(e.type == sfEvtMouseWheelScrolled) retval = OnMouseWheel(cast(MouseWheel) e.mouseWheelScroll.wheel, e.mouseWheelScroll.delta, Vec2(e.mouseWheelScroll.x, e.mouseWheelScroll.y));
	else if(e.type == sfEvtMouseButtonPressed) retval = OnMousePress(cast(MouseButton) e.mouseButton.button, Vec2(e.mouseButton.x, e.mouseButton.y));
	else if(e.type == sfEvtMouseButtonReleased) retval = OnMouseRelease(cast(MouseButton) e.mouseButton.button, Vec2(e.mouseButton.x, e.mouseButton.y));
	else if(e.type == sfEvtMouseMoved) retval = OnMouseMove(Vec2(e.mouseMove.x, e.mouseMove.y));
	else if(e.type == sfEvtMouseEntered) retval = OnMouseEnter();
	else if(e.type == sfEvtMouseLeft) retval = OnMouseLeave();
	else if(e.type == sfEvtJoystickButtonPressed) retval = OnJoystickPress(e.joystickButton.joystickId, e.joystickButton.button);
	else if(e.type == sfEvtJoystickButtonReleased) retval = OnJoystickRelease(e.joystickButton.joystickId, e.joystickButton.button);
	else if(e.type == sfEvtJoystickMoved) retval = OnJoystickMove(e.joystickMove.joystickId, cast(JoystickAxis) e.joystickMove.axis, e.joystickMove.position);
	else if(e.type == sfEvtJoystickConnected) retval = OnJoystickConnect(e.joystickConnect.joystickId);
	else if(e.type == sfEvtJoystickDisconnected) retval = OnJoystickDisconnect(e.joystickConnect.joystickId);
	else if(e.type == sfEvtTouchBegan) retval = OnTouchPress(e.touch.finger, Vec2(e.touch.x, e.touch.y));
	else if(e.type == sfEvtTouchMoved) retval = OnTouchMove(e.touch.finger, Vec2(e.touch.x, e.touch.y));
	else if(e.type == sfEvtTouchEnded) retval = OnTouchRelease(e.touch.finger, Vec2(e.touch.x, e.touch.y));
	else retval = InvalidEvent();
	return retval;
}

template handle(handlers...)
{
	auto ref handle(Event e)
	{
		return e.match!(handlers, function(_) { });
	}
}
