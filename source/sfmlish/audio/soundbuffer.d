module sfmlish.audio.soundbuffer;

import std.conv;
import std.string;

import sfmlish.csfml;
import sfmlish.system.inputstream;
import sfmlish.system.exception;

class SoundBuffer
{
	private sfSoundBuffer* t;

	this(sfSoundBuffer *b) { t = b; }

	static SoundBuffer fromFile(string path)
	{
		auto result = sfSoundBuffer_createFromFile(path.toStringz);
		if(result is null) throw new SFMLException("Failed to load sound from file '" ~ path ~ "'.");
		return new SoundBuffer(result);
	}
	static SoundBuffer fromMemory(const(ubyte)[] bytes)
	{
		auto result = sfSoundBuffer_createFromMemory(cast(const(void)*) bytes.ptr, bytes.length);
		if(result is null) throw new SFMLException("Failed to load sound from memory.");
		return new SoundBuffer(result);
	}
	static SoundBuffer fromStream(InputStream stream)
	{
		sfInputStream s = stream.toSfInputStream;
		auto result = sfSoundBuffer_createFromStream(&s);
		if(result is null) throw new SFMLException("Failed to load sound from input stream " ~ stream.to!string ~ ".");
		return new SoundBuffer(result);
	}
	static SoundBuffer fromSamples(const(short)[] samples, uint channels, uint samplerate)
	{
		auto result = sfSoundBuffer_createFromSamples(samples.ptr, samples.length.to!ulong, channels, samplerate);
		if(result is null) throw new SFMLException("Failed to load sound from samples.");
		return new SoundBuffer(result);
	}

	SoundBuffer dup() const { return new SoundBuffer(sfSoundBuffer_copy(t)); }

	bool saveToFile(string path) const { return sfSoundBuffer_saveToFile(t, path.toStringz).to!bool; }

	@property ulong sampleCount() const { return sfSoundBuffer_getSampleCount(t); }
	@property uint channels() const { return sfSoundBuffer_getChannelCount(t); }
	@property float duration() const { return sfTime_asSeconds(sfSoundBuffer_getDuration(t)); }

	@property sfSoundBuffer *internalPtr() { return t; }
}
