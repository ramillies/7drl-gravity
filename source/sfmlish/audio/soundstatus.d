module sfmlish.audio.soundstatus;

enum SoundStatus { Stopped, Paused, Playing }
