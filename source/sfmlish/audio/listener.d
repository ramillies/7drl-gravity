module sfmlish.audio.listener;

import sfmlish.csfml;
import sfmlish.system.vec;

class Listener
{
	static @property float globalVolume() { return sfListener_getGlobalVolume(); }
	static @property void globalVolume(float f) { return sfListener_setGlobalVolume(f); }
	static @property Vec3 spatialPosition() { return Vec3.fromVector3f(sfListener_getPosition()); }
	static @property void spatialPosition(Vec3 pos) { sfListener_setPosition(pos.toVector3f); }
	static @property Vec3 direction() { return Vec3.fromVector3f(sfListener_getDirection()); }
	static @property void direction(Vec3 dir) { sfListener_setDirection(dir.toVector3f); }
	static @property Vec3 upDirection() { return Vec3.fromVector3f(sfListener_getUpVector()); }
	static @property void upDirection(Vec3 dir) { sfListener_setUpVector(dir.toVector3f); }
}
