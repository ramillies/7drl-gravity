module sfmlish.audio.sound;

import std.conv;

import sfmlish.csfml;
import sfmlish.audio.soundbuffer;
import sfmlish.audio.soundstatus;
import sfmlish.audio.mixins;
import sfmlish.system.vec;
import sfmlish.game.resources;

import std.typecons: Rebindable;

class Sound
{
	private Rebindable!(const(SoundBuffer)) currentBuffer;

	this() { t = sfSound_create(); }
	this(sfSound *b) { t = b; }
	~this() { sfSound_destroy(t); }

	mixin(soundMixinForType("sfSound")); 

	@property void buffer(const(SoundBuffer) b) { sfSound_setBuffer(t, (cast(SoundBuffer) b).internalPtr); currentBuffer = b; }
	@property const(SoundBuffer) buffer() const { return currentBuffer; }

	@property void buffer(string name) { buffer = Sounds.get(name); }
}
