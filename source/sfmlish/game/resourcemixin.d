module sfmlish.game.resourcemixin;

mixin template ResourceStoreInnerWorkings(QualifiedResource, ResourceInfo, alias loadResource)
{
	import sfmlish.game.config;
	import sfmlish.game.logging;
	import sfmlish.system.inputstream;
	import sfmlish.system.exception;

	import std.typecons;
	import std.traits;
	import std.stdio;
	import std.array;

	static if(is(QualifiedResource == const))
	{
		alias Resource = Unconst!QualifiedResource;
		alias ResourceArrayType = Rebindable!(QualifiedResource);
	}
	else
	{
		alias Resource = QualifiedResource;
		alias ResourceArrayType = QualifiedResource;
	}

	private
	{
		static ResourceArrayType[string] resources;
		static string[][string] loadedConfigs;
		static ResourceInfo[string] resourceInfos;
		static ResourceArrayType missing;
		static ResourceInfo missingInfo;
	}

	static void load(string configName)
	{
		if(configName in loadedConfigs)
		{
			sfmlishLogger.errorf(`Refused to load %ss from config file "%s" that has already been loaded.`, Resource.stringof, configName);
			return;
		}

		if(!Config.exists(configName))
		{
			sfmlishLogger.errorf(`Refused to load %ss from a non-existent config file "%s".`, Resource.stringof, configName);
			return;
		}

		auto configContents = Config.parseConfigAs!ResourceInfo(configName);
		auto loadedResources = appender!(string[]);

		foreach(name, info; configContents)
		{
			if(!GameData.exists(info.path))
			{
				sfmlishLogger.errorf(`Trying to load a %s from a non-existing path '%s'.`, Resource.stringof, info.path);
				continue;
			}

			if(name in resources)
			{
				sfmlishLogger.errorf(`Refused to override the %s "%s" from config file "%s" with %s "%s" from config file "%s"`,
					Resource.stringof, name, resourceInfos[name].configName,
					Resource.stringof, name, configName
				);
				continue;
			}
			
			try
			{
				loadResource(name, info);
				info.configName = configName;
				loadedResources.put(name);
			}
			catch(SFMLException e)
			{
				sfmlishLogger.errorf(`Could not load the %s '%s' from file '%s': %s`, Resource.stringof, name, info.path, e.msg); 
				continue;
			}
		}

		loadedConfigs[configName] = loadedResources.data;
	}

	static void unload(string configName)
	{
		if(!(configName in loadedConfigs))
		{
			sfmlishLogger.errorf(`Cannot unload %ss from the config "%s" because it was not loaded in the first place.`, Resource.stringof, configName);
			return;
		}
		foreach(name; loadedConfigs[configName])
		{
			resources[name].destroy();
			resources.remove(name);
			resourceInfos.remove(name);
		}
		loadedConfigs.remove(configName);
	}

	static bool exists(string name) { return (name in resources) ? true : false; }
	static ResourceArrayType get(string name)
	{
		if(exists(name)) return resources[name];
		else
		{
			sfmlishLogger.warningf(`There is no resource "%s" in the currently loaded config files %(%s, %).`, Resource.stringof, loadedConfigs.byKey);
			return missing;
		}
	}
	static ResourceInfo info(string name)
	{
		if(exists(name)) return resourceInfos[name];
		else
		{
			sfmlishLogger.warningf(`There is no resource "%s" in the currently loaded config files %(%s, %).`, Resource.stringof, loadedConfigs.byKey);
			return missingInfo;
		}
	}
}
