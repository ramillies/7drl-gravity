module sfmlish.game.appwindow;

import sfmlish.graphics.renderwindow;
import sfmlish.game.screen;
import sfmlish.window.window;
import sfmlish.window.event;
import sfmlish.window.contextsettings;
import sfmlish.window.videomode;
import sfmlish.system.clock;

import std.range;

class AppWindow: RenderWindow
{
	private Screen[] screens;
	private Screen superscreen;
	private Clock[] screenClocks;

	this(VideoMode mode, string title, WindowStyle style = WindowStyle.Default, ContextSettings settings = ContextSettings())
	{
		super(mode, title, style, settings);
	}

	void pushScreen(Screen s)
	{
		s.setWindow(this);
		screens ~= s;
		screenClocks ~= new Clock;

		s.initialize;
	}

	void popScreen()
	{
		if(!screens.empty)
		{
			screens[$-1].finish;
			screens = screens[0 .. $-1];
			screenClocks = screenClocks[0 .. $-1];
		}
	}

	void popAllScreens()
	{
		while(!screens.empty)
			popScreen();
	}

	void popScreensUntil(Screen s)
	{
		while(screens[$-1] !is s && !screens.empty)
			popScreen();
	}

	void setSuperScreen(Screen s) { superscreen = s; }
	void removeSuperScreen() { superscreen = null; }
	bool hasSuperScreen() { return !(superscreen is null); }

	void loopStep()
	{
		if(!this.isOpen) return;

		Event e;
		while(this.pollEvent(e))
		{
			if(!screens.empty)
				screens[$-1].event(e);
			if(hasSuperScreen())
				superscreen.event(e);
		}

		foreach(n, screen; screens)
			if(n == screens.length - 1)
				screen.update(screenClocks[n].restart);
			else
				screen.updateInactive(screenClocks[n].restart);

		this.clear;
		foreach(s; screens)
			s.draw;
		if(hasSuperScreen()) superscreen.draw;
		this.display;
	}
}
