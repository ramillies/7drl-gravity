module sfmlish.game.screen;

import sfmlish.game.appwindow;
import sfmlish.window.event;

interface Screen
{
	void initialize();
	void setWindow(AppWindow win);
	void event(Event e);
	void update(double dt);
	void updateInactive(double dt);
	void draw();
	void finish();
}
