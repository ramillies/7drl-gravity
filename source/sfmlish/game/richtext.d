module sfmlish.game.richtext;

import sfmlish.graphics.font;
import sfmlish.graphics.color;
import sfmlish.graphics.transform;
import sfmlish.graphics.transformable;
import sfmlish.graphics.rendertarget;
import sfmlish.graphics.renderstates;
import sfmlish.graphics.drawable;
import sfmlish.callbackmixins;
import sfmlish.system.vec;
import sfmlish.system.rect;
import sfmlish.game.resources;
import sfmlish.game.text.layout;
import sfmlish.game.text.markup;
import sfmlish.game.text.parser;
import sfmlish.game.text.renderer;

import std.typecons: Rebindable;
import std.algorithm;
import std.range;
import std.stdio;
import std.string;

class RichText: Transformable, Drawable, Bboxable
{
	private Rebindable!(const(Font)) currentFont;
	private uint size;
	private Color textColor;
	private Rect bbox;
	private string markupString;
	private Transform transform;
	private float boxSize;
	private MarkupRenderer renderer;

	this()
	{
		size = 30;
		textColor = Color.White;
		bbox = Rect(0,0,0,0);
		transform = new Transform;
		boxSize = 0;
		renderer = new MarkupRenderer;
	}

	@property const(Font) font() const { return currentFont; }
	@property void font(const(Font) f) { currentFont = f; updateVertices; }
	@property void font(string name) { currentFont = Fonts.get(name); updateVertices; }

	@GenerateCallback
	{
		@property uint fontSize() const { return size; }
		@property void fontSize(uint s) { size = s; updateVertices;  }
		@property Color color() const { return textColor; }
		@property void color(Color c) { textColor = c; updateVertices; }
		@property string markup() const { return markupString; }
		@property void markup(string s) { markupString = s; updateVertices; }
		@property Vec2 pos() const { return transform.pos; }
		@property float rotation() const { return transform.rotation; }
		@property Vec2 scale() const { return transform.scale; }
		@property Vec2 origin() const { return transform.origin; }
		@property void pos(Vec2 v) { transform.pos = v; }
		@property void rotation(float f) { transform.rotation = f; }
		@property void scale(Vec2 v) { transform.scale = v; }
		@property void origin(Vec2 v) { transform.origin = v; }
		@property float textBoxWidth() const { return boxSize; }
		@property void textBoxWidth(float f) { boxSize = f; updateVertices; }
	}

	@property Rect localBounds() const { return bbox; }
	@property Matrix matrix() const { return transform.matrix; }
	@property Matrix inverseMatrix() const { return transform.inverseMatrix; }

	mixin(relativeOriginMixin);
	mixin(transformableBbox);

	override void draw(RenderTarget target, RenderStates states)
	{
		states.transform = transform.matrix * states.transform;
		renderer.draw(target, states);
	}

	private void updateVertices()
	{
		if(font is null) return;
		bbox = Rect();
		if(markupString == "") return;

		MarkupParser parser = MarkupParser(font, color, size, boxSize);

		MarkupLayout layout = MarkupLayout(boxSize);
		layout.layOutElements(parser.parseMarkup(markupString));

		renderer.renderSymbols(layout.symbols);
		renderer.renderLines(layout.lines);
		bbox = renderer.bbox;
	}
}
