module sfmlish.game.text.markup;

import sfmlish.graphics.color;
import sfmlish.graphics.font;
import sfmlish.graphics.glyph;
import sfmlish.graphics.texture;
import sfmlish.game.logging;
import sfmlish.game.resources;
import sfmlish.system.vec;
import sfmlish.system.rect;

import std.algorithm;
import std.array;
import std.conv;
import std.range;
import std.regex;
import std.stdio;
import std.sumtype;
import std.typecons;
import std.uni;

enum Alignment { Left, Right, Center, Justify }

struct Symbol
{
	const(Texture) texture;
	float advance;
	Rect bounds, textureRect;
	Color color;
	float outlineThickness, raiseHeight, lineHeight, letterSpacing;
	Vec2 pos;
}

struct Breakpoint { Symbol[] symbolsAtBreak; } // A point where a line break is possible
struct HSpace { float advance; } // Horizontal empty space
struct VSpace { float height; } // Vertical empty space
struct Align { Alignment alignment; }
struct TabJump { float x; } // Jump to given tab
struct Underline
{
	bool on;
	Color fillColor, outlineColor;
	float thickness, outlineThickness;
	float height;
	float xPos;
}
struct Strikethrough
{
	bool on;
	Color fillColor, outlineColor;
	float thickness, outlineThickness;
	float height;
	float xPos;
}
struct Newline { }

alias MarkupElement = SumType!(Symbol, Breakpoint, HSpace, VSpace, Align, TabJump, Newline, Underline, Strikethrough);

struct DrawLine
{
	Rect pos;
	Color color;
}

public bool isWhitespace(MarkupElement e) { return e.match!((HSpace _) => true, (_) => false); }
public bool isSymbol(MarkupElement e) { return e.match!((Symbol _) => true, (_) => false); }
public bool isBreakpoint(MarkupElement e) { return e.match!((Breakpoint _) => true, (_) => false); }
public bool isUnderline(MarkupElement e) { return e.match!((Underline _) => true, (_) => false); }
public bool isStrikethrough(MarkupElement e) { return e.match!((Strikethrough _) => true, (_) => false); }
public float tabJump(MarkupElement e) { return e.match!((TabJump j) => j.x, (_) => 0); }
public float advance(MarkupElement e)
{
	return e.match!(
		(HSpace h) => h.advance,
		(Symbol s) => s.advance,
		(_) => 0
	);
}

