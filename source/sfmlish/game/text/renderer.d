module sfmlish.game.text.renderer;

import sfmlish.graphics.drawable;
import sfmlish.graphics.glyph;
import sfmlish.graphics.vertexarray;
import sfmlish.graphics.vertex;
import sfmlish.graphics.primitivetype;
import sfmlish.graphics.color;
import sfmlish.graphics.texture;
import sfmlish.graphics.rendertarget;
import sfmlish.graphics.renderstates;
import sfmlish.system.vec;
import sfmlish.system.rect;
import sfmlish.game.text.markup;

import std.typecons: Rebindable;
import std.algorithm;
import std.range;
import std.stdio;
import std.string;

class MarkupRenderer: Drawable
{
	private VertexArray[uint] vertices;
	private VertexArray lineVertices;
	private Rebindable!(const(Texture))[uint] textures;
	private Rect boundingBox;

	this() { lineVertices = new VertexArray; }

	private void addGlyphQuad(VertexArray where, Glyph g, Vec2 pos, Color color)
	{
		float padding = 1.;
		Rect glyphBounds = Rect(g.bounds.left - padding, g.bounds.top - padding, g.bounds.width + 2*padding, g.bounds.height + 2*padding);
		Rect textureBounds = Rect(g.textureRect.left - padding, g.textureRect.top - padding, g.textureRect.width + 2*padding, g.textureRect.height + 2*padding);

		foreach(corner; [ Vec2(0,0), Vec2(0,1), Vec2(1,1), Vec2(1,0) ])
			where.append(Vertex(pos + glyphBounds.relativePoint(corner), color, textureBounds.relativePoint(corner)));
	}

	void prepare() { boundingBox = Rect(0,0,0,0); }

	void renderSymbols(Symbol[] symbols)
	{
		float minX = boundingBox.left, minY = boundingBox.top, maxX = boundingBox.right, maxY = boundingBox.bottom;

		vertices.clear;
		textures.clear;

		foreach(s; symbols[])
		{
			uint textureId = s.texture.nativeHandle;
			if(textureId !in vertices)
			{
				vertices[textureId] = new VertexArray;
				vertices[textureId].primitiveType = PrimitiveType.Quads;
				textures[textureId] = s.texture;
			}
			addGlyphQuad(vertices[textureId], Glyph(s.advance, s.bounds, s.textureRect), s.pos, s.color);
			minX = min(minX, s.pos.x + s.bounds.left);
			minY = min(minY, s.pos.y + s.bounds.top);
			maxX = max(maxX, s.pos.x + s.bounds.right);
			maxY = max(maxY, s.pos.y + s.bounds.bottom);
		}
		boundingBox = Rect(minX, minY, maxX - minX, maxY - minY);
	}

	void renderLines(DrawLine[] lines)
	{
		float minX = boundingBox.left, minY = boundingBox.top, maxX = boundingBox.right, maxY = boundingBox.bottom;

		lineVertices.clear;
		lineVertices.primitiveType = PrimitiveType.Quads;
		foreach(line; lines)
		{
			foreach(corner; [ Vec2(0,0), Vec2(0,1), Vec2(1,1), Vec2(1,0) ])
				lineVertices.append(Vertex(line.pos.relativePoint(corner), line.color, Vec2(1,1)));
			minX = min(minX, line.pos.left);
			minY = min(minY, line.pos.top);
			maxX = max(maxX, line.pos.right);
			maxY = max(maxY, line.pos.bottom);
		}
		boundingBox = Rect(minX, minY, maxX - minX, maxY - minY);
	}

	void draw(RenderTarget target, RenderStates states) 
	{
		foreach(textureId; vertices.byKey)
		{
			states.texture = textures[textureId];
			target.draw(vertices[textureId], states);
		}
		states.texture = null;
		target.draw(lineVertices, states);
	}

	@property Rect bbox() const { return boundingBox; }
}
