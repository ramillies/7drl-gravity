module sfmlish.game.text.parser;

import sfmlish.graphics.color;
import sfmlish.graphics.font;
import sfmlish.graphics.glyph;
import sfmlish.graphics.texture;
import sfmlish.game.logging;
import sfmlish.game.resources;
import sfmlish.system.vec;
import sfmlish.system.rect;
import sfmlish.game.text.markup;

import std.algorithm;
import std.array;
import std.conv;
import std.format;
import std.range;
import std.regex;
import std.stdio;
import std.sumtype;
import std.typecons;
import std.uni;

private Color parseColor(string color)
{
	const presetColors = [ "black": Color.Black, "white": Color.White, "red": Color.Red, "green": Color.Green,
	      "blue": Color.Blue, "cyan": Color.Cyan, "magenta": Color.Magenta, "yellow": Color.Yellow ];
	if(auto r = color in presetColors)
		return *r;
	if(auto m = color.matchFirst(ctRegex!`^[0-9A-Fa-f]{8}$`))
	{
		auto hexRGBA = m[0].to!int(16);
		return Color(
			to!ubyte( (hexRGBA>>24) & 255 ),
			to!ubyte( (hexRGBA>>16) & 255 ),
			to!ubyte( (hexRGBA>>8) & 255 ),
			to!ubyte( hexRGBA & 255 )
		);
	}
	if(auto m = color.matchFirst(ctRegex!`^[0-9A-Fa-f]{6}$`))
	{
		auto hexRGBA = m[0].to!int(16);
		return Color(
			to!ubyte( (hexRGBA>>16) & 255 ),
			to!ubyte( (hexRGBA>>8) & 255 ),
			to!ubyte( hexRGBA & 255 ),
			255
		);
	}
	else throw new ConvException(`Could not convert '` ~ color ~ `' to a color.`);
}

private string propertiesForSymbol(string type, string symbol, Flag!"cancelKerning" cancelKerning = Yes.cancelKerning)
{
	return format(`@property %1$s %2$s() const { return _%2$s; }`, type, symbol) ~
	format(`@property void %2$s(%1$s val) 
	{
		if(_%2$s == val) return;` ~
		((cancelKerning == Yes.cancelKerning) ? `thisChar = 0;` : ``) ~
	`	_%2$s = val;
		if(%3$s || underline) addLine!Underline;
		if(%4$s || strikethrough) addLine!Strikethrough;
	}`, type, symbol, symbol == "underline", symbol == "strikethrough");
}

private struct IconParameters
{
	Color color = Color(255,255,255,255);
	dchar mimic = 0;
	bool fullHeight = false;
	bool allowStretching = false;
	float raisePercent = 0.;
}

private IconParameters parseIconParameters(string params)
{
	IconParameters result;
	foreach(param; params.splitter(regex(`, *`)))
	{
		try
		{
			if(auto m = param.matchFirst(regex(`color=(.*)`)))
				result.color = parseColor(m[1]);
			else if(auto m = param.matchFirst(regex(`as=(.)`)))
				result.mimic = m[1].to!(dchar[]).front;
			else if(auto m = param.matchFirst(regex(`raise=([-0-9.]+)%`)))
				result.raisePercent = m[1].to!float;
			else if(param == "full-height")
				result.fullHeight = true;
			else if(param == "allow-stretching")
				result.allowStretching = true;
			else
			{
				sfmlishLogger.warningf(`Skipping invalid icon parameter '%s' in rich text markup.`, param);
				continue;
			}
		}
		catch(ConvException e)
		{
			sfmlishLogger.warningf(`Skipping invalid icon parameter '%s' in rich text markup: %s`, param, e.msg);
			continue;
		}
	}
	return result;
}


struct MarkupParser
{
	const(Font) baseFont;
	Color baseColor;
	uint baseSize;
	float boxSize = 0.;

	private
	{
		Rebindable!(const(Font)) _font;
		Color _color;
		uint _size;
		Color _outlineColor;
		float _outlineThickness, _raiseHeight, _letterSpacing;
		bool _underline, _strikethrough;

		Appender!(MarkupElement[]) result, paragraph;
		dchar thisChar, lastChar;

		mixin(propertiesForSymbol("const(Font)", "font"));
		mixin(propertiesForSymbol("Color", "color", No.cancelKerning));
		mixin(propertiesForSymbol("uint", "size"));
		mixin(propertiesForSymbol("Color", "outlineColor", No.cancelKerning));
		mixin(propertiesForSymbol("float", "outlineThickness", No.cancelKerning));
		mixin(propertiesForSymbol("float", "raiseHeight"));
		mixin(propertiesForSymbol("float", "letterSpacing"));
		mixin(propertiesForSymbol("bool", "underline", No.cancelKerning));
		mixin(propertiesForSymbol("bool", "strikethrough", No.cancelKerning));
	}


	Symbol[] makeSymbols(dchar character)
	{
		auto result = appender!(Symbol[]);
		if(outlineThickness != 0)
		{
			Glyph g = font.glyph(character, size, false, outlineThickness);
			// Add an outline as an another glyph with zero advance.
			// That will cause the actual glyph to be rendered right on top of the outline.
			result ~= Symbol(
				font.texture(size),
				0, g.bounds, g.textureRect,
				outlineColor,
				outlineThickness,
				raiseHeight,
				font.lineSpacing(size),
				0
			);
		}
		Glyph g = font.glyph(character, size, false, 0);
		result ~= Symbol(
			font.texture(size),
			g.advance, g.bounds, g.textureRect,
			color,
			0,
			raiseHeight,
			font.lineSpacing(size),
			0
		);

		return result[];
	}

	private void addLine(T)() if(is(T == Underline) || is(T == Strikethrough))
	{
		paragraph ~= MarkupElement(T(
			is(T == Underline) ? underline : strikethrough,
			color, outlineColor,
			font.underlineThickness(size), outlineThickness,
			- raiseHeight + 
				(is(T == Underline)
				 	? font.underlinePos(size)
					: font.glyph('x', size).bounds.relativePoint(Vec2(0.,0.5)).y
				)
		));
	}

	private void cancelKerning() { thisChar = 0; }

	Rect defaultIconRect()
	{
		Glyph mimicGlyph = font.glyph('M', size, false, 0);
		Rect targetRect = mimicGlyph.bounds;
		targetRect.top -= 0.2 * targetRect.height;
		targetRect.height *= 1.2;
		return targetRect;
	}

	MarkupElement[] parseMarkup(string markup)
	{
		font = baseFont;
		color = baseColor;
		size = baseSize;
		outlineColor = Color.Black;
		outlineThickness = 0.; raiseHeight = 0.; letterSpacing = 0.;
		underline = false; strikethrough = false;

		result = appender!(MarkupElement[]);
		paragraph = appender!(MarkupElement[]);

		thisChar = 0; lastChar = 0;

		auto tokenRegexes = regex([	// One token is:
			`\{[^}]*\}`,		// 1) a sequence of characters enclosed in braces;
			`\n+`,			// 2) any number of newlines;
			` +`,			// 3) any number of spaces;
			`.`,			// 4) or any one other symbol.
		], "s");
		auto tokens = markup.splitter!(Yes.keepSeparators)(tokenRegexes).filter!((t) => t != "");

		foreach(token; tokens)
		{
			if(token[0] == ' ')
				paragraph ~= [ MarkupElement(Breakpoint()), MarkupElement(HSpace(font.glyph(' ', size).advance)) ];
			else if(token == "\t")
				paragraph ~= [ MarkupElement(Breakpoint()), MarkupElement(HSpace(4*font.glyph(' ', size).advance)) ];
			else if(token[0] == '\n')
			{
				result ~= paragraph[];
				paragraph.clear;
				result ~= MarkupElement(Newline());
			}
			else if(token[0] == '{')
			{
				try
				{
					if(token == "{left-align}")
						result ~= MarkupElement(Align(Alignment.Left));
					else if(token == "{right-align}")
						result ~= MarkupElement(Align(Alignment.Right));
					else if(token == "{center}")
						result ~= MarkupElement(Align(Alignment.Center));
					else if(token == "{justify}")
						result ~= MarkupElement(Align(Alignment.Justify));
					else if(auto m = token.matchFirst(regex(`^\{vspace ([-0-9.]+)%\}$`)))
						result ~= MarkupElement(VSpace(m[1].to!float / 100. * baseSize));
					else if(auto m = token.matchFirst(regex(`^\{color (.*)\}$`)))
						color = parseColor(m[1]);
					else if(auto m = token.matchFirst(regex(`^\{size ([-0-9.]+)%\}$`)))
						size = to!uint(m[1].to!float / 100. * baseSize);
					else if(auto m = token.matchFirst(regex(`^\{outline-color (.*)\}$`)))
						outlineColor = parseColor(m[1]);
					else if(auto m = token.matchFirst(regex(`^\{outline-thickness ([0-9.]+)%\}$`)))
						outlineThickness = max(1, to!int(.5 + m[1].to!float / 100. * baseSize));
					else if(auto m = token.matchFirst(regex(`^\{font (.*)\}$`)))
						font = Fonts.get(m[1]);
					else if(auto m = token.matchFirst(regex(`^\{underline (on|off)\}$`)))
						underline = m[1] == "on";
					else if(auto m = token.matchFirst(regex(`^\{strikethrough (on|off)\}$`)))
						strikethrough = m[1] == "on";
					else if(auto m = token.matchFirst(regex(`^\{hspace ([-0-9.]+)%\}$`)))
					{
						paragraph ~= [ MarkupElement(Breakpoint()), MarkupElement(HSpace(m[1].to!float / 100. * baseSize)) ];
						cancelKerning;
					}
					else if(auto m = token.matchFirst(regex(`^\{nb-hspace ([-0-9.]+)%\}$`)))
					{
						paragraph ~= MarkupElement(HSpace(m[1].to!float / 100. * baseSize));
						cancelKerning;
					}
					else if(token == "{~}")
					{
						paragraph ~= MarkupElement(HSpace(font.glyph(' ', size).advance));
						cancelKerning;
					}
					else if(token == "{-}")
						paragraph ~= MarkupElement(Breakpoint(makeSymbols('-')));
					else if(auto m = token.matchFirst(regex(`^\{raise ([-0-9.]+)%\}$`)))
						raiseHeight = m[1].to!float / 100. * baseSize;
					else if(token == "{reset}")
					{
						font = baseFont;
						color = baseColor;
						size = baseSize;
						raiseHeight = 0;
						outlineThickness = 0;
						underline = false;
						strikethrough = false;
					}
					else if(token == "{{}")
					{
						paragraph ~= makeSymbols('{').map!((s) => MarkupElement(s));
						lastChar = '{';
					}
					else if(auto m = token.matchFirst(regex(`^\{tab ([-0-9.]+)%\}$`)))
					{
						paragraph ~= MarkupElement(TabJump(m[1].to!float / 100. * boxSize));
						cancelKerning;
					}
					else if(auto m = token.matchFirst(regex(`^\{icon(\(([^\)]*)\))? ((.+)/)?([^/]+)\}$`)))
					{
						auto paramList = m[2], textureName = m[4], iconName = m[5];
						IconParameters params = parseIconParameters(paramList);
						Rect textureRect;
						if(auto m2 = iconName.matchFirst(regex(`\d+`)))
							textureRect = Textures.tileForTexture(textureName, iconName.to!int);
						else
							textureRect = Textures.tileForTexture(textureName, iconName);

						Rect targetRect = (params.mimic == 0) ? defaultIconRect : font.glyph(params.mimic, size).bounds;
						if(!params.allowStretching)
						{
							if(textureRect.height > textureRect.width)
							{
								auto newWidth = textureRect.width * targetRect.height/textureRect.height;
								targetRect = Rect(targetRect.left, targetRect.top, newWidth, targetRect.height);
							}
							else
							{
								auto newHeight = textureRect.height * targetRect.width/textureRect.width;
								targetRect = Rect(targetRect.left, targetRect.bottom - newHeight, targetRect.width, newHeight);
							}
						}
						if(params.fullHeight)
						{
							Glyph g = font.glyph('g', size);
							auto newHeight = font.lineSpacing(size);
							auto newWidth = textureRect.width * newHeight/textureRect.height;
							targetRect = Rect(g.bounds.left, g.bounds.bottom - newHeight, newWidth, newHeight);
						}
						paragraph ~= MarkupElement(Symbol(
							Textures.get(textureName),
							targetRect.width, targetRect, textureRect,
							params.color,
							0,
							raiseHeight + params.raisePercent*size/100.,
							font.lineSpacing(size),
							0
						));
					}
					else
						sfmlishLogger.errorf(`Skipped an unknown control sequence '%s' in markup.`, token);

				}
				catch(ConvException e)
				{
					sfmlishLogger.errorf(`Skipped an invalid control sequence '%s' in markup: %s.`, token, e.msg);
					continue;
				}
			}
			else
			{
				thisChar = token.to!(dchar[]).front;
				auto kern = font.kerning(lastChar, thisChar, size);
				if(kern != 0)
					paragraph ~= MarkupElement(HSpace(kern));
				paragraph ~= makeSymbols(thisChar).map!((s) => MarkupElement(s));
				lastChar = thisChar;
			}
		}

		result ~= paragraph.data;
		return result.data;
	}
}
