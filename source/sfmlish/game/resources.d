module sfmlish.game.resources;

import sfmlish.graphics.texture;
import sfmlish.graphics.rendertexture;
import sfmlish.graphics.font;
import sfmlish.graphics.color;
import sfmlish.audio.soundbuffer;
import sfmlish.audio.music;
import sfmlish.game.datastore;
import sfmlish.game.logging;
import sfmlish.game.resourcemixin;
import sfmlish.system.rect;

import std.typecons;
import std.range;
import std.array;
import std.conv;
import std.base64;
import std.zlib;

struct TextureInfo
{
	string path;
	int[] tileSize;
	int tileCount = 1;
	bool smooth = true;
	bool repeated = false;
	int[string] tileNames;
	Rect[string] rectangleNames;
	string configName;
}

struct FontInfo
{
	string path;
	string configName;
}

struct SoundInfo
{
	string path;
	string configName;
}

struct MusicInfo
{
	string path;
	bool loopWhole = false;
	float[] loopPoints;
	string configName;
}

// This is a little FLAC file with a bit of silence, embedded in the source.
// It's used as a "miss" for the sound storing classes.
private enum silence = Base64.decode(
`ZkxhQwAAACIQABAAAAAKAAAMC7gAcAAAEsDnCTh7JO6yjCG2DlubZjnvhAAAKCAAAAByZWZlcmVu` ~
`Y2UgbGliRkxBQyAxLjMuNCAyMDIyMDIyMAAAAAD/+MoCAKoAAD2e//h6AgECvy4AAMhw`
);

// A little "miss" texture (a red square).
private enum missTexture = Base64.decode(
"iVBORw0KGgoAAAANSUhEUgAAAEAAAABAAQMAAACQp+OdAAAABlBMVEX/AAD///9BHTQRAAAAD0lE" ~
"QVQoz2NgGAWjgHwAAAJAAAGMxat3AAAAAElFTkSuQmCC"
);


// A little "miss" font. Sadly, it's a lot bigger. In order to save a bit of space,
// it was gzipped.

private enum missFont = Base64.decode(
`eNrdVmtsVNcRnnu/M2vv2mvvehdiB2zW2Fvb8TNrEsAYcm3WPFNwcKAmwcGG9QO/sc0rZBNTgniY` ~
`d1JjSJo4AamtaVPTqsTBqDUp9CHqqhJCJK2qNIpQValV2x+tVFe627m7C0pb9X/UM3vu/WbOzDcz` ~
`x7LukEZEKTRIoMb1daWBrst/PkiktYl10/aupl5Ktd0W/Veyq7fvHvCd2nOFiexyrI209LZ2/X7w` ~
`rzeJ4BXDUGtTfy8liFByvujJrZ37Wj767epRIscl0fPbmptCc66+L/H0R9lPtonBfsc2X7hyRc9t` ~
`6xrY65xNQdGnRHd19mxvonwRcoyI7uhq2turCuiinK8S3dfd1NVcE8mUDhy/ENuS3p7+gchv6HlJ` ~
`nWid9/Y19+7aZ/u71Cc18Atk9Sr70vy0F7emVv6Nkiw3ol//6a3D1vsT/ZdFVnY9xBtFTSSdYkti` ~
`ErrMTMtkLT0UZfr8UlGLehijk4vOWzehkh5w6EPaaZLeuZzPi5oVe+MetehpEmCzgW2s6+o+2SIb` ~
`6B//dCjyWYG1LTUhMgT7bF7Tq11I6NI+ayR6+5N70URDVsWxygRbC/E9N15lpWhaVI8xqni1vkgk` ~
`9ox89qAfzUYPW9P0aB//vrQY9f/3umpjBV2jIp9rXPevDo0bz9T7frY5u7joP1SfK8E3TrXjzn2+` ~
`iUiktl7N4c3jPHcc/sRx5c/59H8dflpctLa23jf+85pgnLWmMSi2unqBliZmsdcEi2lCHzR+91MT` ~
`P/Hg1s1cvhXCLUPdzMWPnfjwRh5/GMKNPEw14EdhXE/CZBKufeDlawF84MVEAO+buGriBya+b+J7` ~
`Jq6Mr+QrMxhfie+aeC+M75j4dgoujyXzZS/GkvGtAL4ZwjeycCmAi++G+KKJd0N4ZziF3/FjdK+D` ~
`R/14ey3ecuHrJXjzSBa/aeKNCy5+IxMXXDg/ksLn/RgRv5EUjBjqnASe8+LcoBpOwbChvubH66+W` ~
`8esmXjvr4df8OHvGyWc9ODuhGYZdnTnt4DNOnJnQyFitTjtwekqd6gnzqUmcPJDEJ904aagTgk5U` ~
`4PjQJB83MXSsgYcmMTSojh3187EGHDPUUanrqB9HDrv5SBaOTESmjIg67MYhSX0ohFfLcHA2vjqM` ~
`A0kYDIV40MQrnS5+JQMvh1P45QDCKXhpfyq/5MH+VLw4jH1u7HVgz24f75nB7l1zebcPu+ZiQIIG` ~
`stBvos/Ezl4n7zTR60SvoXrC6O5axt0d6FqGzo5k7nShc1B1JKPDUO2Ssn0GO9omeYeJttYGbptE` ~
`26BqbfFzawNaDdXiR7M4Nc8gFML2WdhmoslE49YSbjSxtQQvmGgwsWUtng/jORObg6g38RUTmyax` ~
`0URdCBu8eCaA2vWpXBvG+lSsqzKq8XQS1oSwen4irx7GqgBWwsUrPViRhhrdwTUZCC73cLADy6td` ~
`vNyD6qokrnahyrBzVRIMOwzrHuvUU8NYpop52ZextNLLS9eicomDK72oNNQSByoWp3FFAxYvcvPi` ~
`NCxyY6ETT5p4YoGXnzCxoNzDC7woDzi43IPA43YOOBCI/X0et6OsNJ3LgigtmcWl6SidUiVZDi6Z` ~
`hZJBVWwPcfEwigq9XLQWhdJEoReFhnpMSn8shIL8Mi6oQr4Ull+GPHnlmfhSBfzOdPY3IDcnjXPr` ~
`kCNhOWnIMdT8RGT70jm7Ab55bvalwzel5kmyeW7MG1RZDmQZKjMHc1MxJxePZpTxo3XIENaMMqSb` ~
`eESSPmJitguzvF6e1QGvx8NeL7yG8niQJn5pk3DL9bpNuOTlqkaq1J86jBQ5SzHhFAJnOpyGSjaR` ~
`JEqSsagDDvFxhGEPITHBzYleJLhh4wDbwmCJ4wCUkKliCKnugFYHMqFNaKFDJ7TCL+yiwi/yki+6` ~
`5qB++cpN0G2RO4KepnbaSYdpVPB92h+131CJlojlY7Z879CEViB+uiVaafQ7+UPhKZez++LfIvoo` ~
`bYuez2A6Khcwre8hHbWCaqMRozSBJUphOrajUbdpKV2jMQvzNA2L3wa6J1It7GvoOn2sHaRL2l0K` ~
`0wnqV9bHKlNz8F2ppZ228d2o/IW2RDNbtna+a/NKpnbp87qwX4rZtQJtAxrRotVLh7o2hlViPUTt` ~
`qlEkLyrBaH+xHnR9v+SP90vT+ha9QOVpY5LHyjEt/GO0VOptkUpXydat+vEHGkWyVJjBN2hNwhpb` ~
`smZLCNOzcrof5doFWyY1UhjPCsM6sZ2gddo9yUIySgbZTRaIiXVXD6YyWDNVuTvb7c92Zwd1n5mr` ~
`jZhtvHHmclBNRyfZ9z43m+lxrIkkxLEuKCOOQelUF8fWZXbHMcs4dSCOZSKi49YEpexx1hjWhAlx` ~
`rEteexyDyig7ji3OzjhmmQh74tjiPGZRNUd/G+TRSrvEtYn6RG1u3dXZJGCFBHTTQPTdJx7NMr5V` ~
`WINg9BmgBVRMC2UHJGVAhFb0dA+s6OlrbfZV+OQXWFC8sDhQFghEy94k8X1ywTuirDIIbmru69/R` ~
`0+2jh1OjrMhH9BzRf823sZuT/wCZGfEvJlI0Kw==`
);

class Textures
{
	mixin ResourceStoreInnerWorkings!(const Texture, TextureInfo, loadByNameAndInfo);

	static void initialize()
	{
		missing = Texture.fromMemory(missTexture);
		missingInfo = TextureInfo("MISS!!!", [64,64]);
	}

	private static void loadByNameAndInfo(string name, TextureInfo info)
	{
		Texture t = Texture.fromStream(GameData.getFileStream(info.path));
		if(info.tileSize.empty)
			info.tileSize = [ t.size.x.to!int, t.size.y.to!int ];
		t.smooth = info.smooth;
		t.repeated = info.repeated;
		resources[name] = t;
		resourceInfos[name] = info;
	}

	static Rect tileForTexture(string texture, int tile)
	{
		auto textureInfo = Textures.info(texture);
		auto textureSize = Textures.get(texture).size;
		auto tilecount = textureInfo.tileCount;
		auto tilenum = (tile%tilecount + tilecount) % tilecount;

		auto tilesize = textureInfo.tileSize;
		auto tilesPerRow = to!uint(textureSize.x) / tilesize[0];
		int tileX = tilenum % tilesPerRow;
		int tileY = tilenum / tilesPerRow;
		return Rect(tileX * tilesize[0], tileY * tilesize[1], tilesize[0], tilesize[1]);
	}

	static Rect tileForTexture(string texture, string tile)
	{
		auto textureInfo = Textures.info(texture);
		if(tile in textureInfo.rectangleNames)
			return textureInfo.rectangleNames[tile];
		else if(tile in textureInfo.tileNames)
			return Textures.tileForTexture(texture, textureInfo.tileNames[tile]);
		else
		{
			sfmlishLogger.warningf(`The texture "%s" has no tile called "%s".`, texture, tile);
			auto textureSize = Textures.get(texture).size;
			return Rect(0, 0, textureSize.x, textureSize.y);
		}
	}
}


class Fonts
{
	mixin ResourceStoreInnerWorkings!(const Font, FontInfo, loadByNameAndInfo);

	static void initialize()
	{
		missing = Font.fromMemory(cast(ubyte[]) uncompress(missFont));
		missingInfo = FontInfo("MISS!!!");
	}

	private static void loadByNameAndInfo(string name, FontInfo info)
	{
		Font t = Font.fromStream(GameData.getFileStream(info.path));
		resources[name] = t;
		resourceInfos[name] = info;
	}
}

class Sounds
{
	mixin ResourceStoreInnerWorkings!(const SoundBuffer, SoundInfo, loadByNameAndInfo);

	static void initialize()
	{
		missing = SoundBuffer.fromMemory(silence);
		missingInfo = SoundInfo("MISS!!!");
	}

	private static void loadByNameAndInfo(string name, SoundInfo info)
	{
		SoundBuffer t = SoundBuffer.fromStream(GameData.getFileStream(info.path));
		resources[name] = t;
		resourceInfos[name] = info;
	}
}

class Musics
{
	mixin ResourceStoreInnerWorkings!(Music, MusicInfo, loadByNameAndInfo);

	static void initialize()
	{
		missing = Music.fromMemory(silence);
		missingInfo = MusicInfo("MISS!!!");
	}

	private static void loadByNameAndInfo(string name, MusicInfo info)
	{
		Music t = Music.fromStream(GameData.getFileStream(info.path));
		if(info.loopWhole)
			t.loop = true;
		if(info.loopPoints !is null && info.loopPoints.length == 2)
		{
			t.ABLoop = MusicABLoop(info.loopPoints[0], info.loopPoints[1]);
			t.loop = true;
		}
		resources[name] = t;
		resourceInfos[name] = info;
	}

	static ~this()
	{
		foreach(name, music; resources)
			music.destroy();
	}
}
