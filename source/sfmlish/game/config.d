module sfmlish.game.config;

import std.stdio;
import std.array;
import std.traits;

import sfmlish.game.datastore;
import sfmlish.game.logging;

import dyaml;

class Config
{
	private
	{
		static Node[string] configFiles;
	}

	static void parseMasterConfig(string source)
	{
		Node masterConfigRoot = Loader.fromString(source).load;
		foreach(string name, string path; masterConfigRoot)
			configFiles[name] = Loader.fromString(GameData.readFileAsUTF8(path)).load;
	}

	static bool exists(string configName) { return (configName in configFiles) ? true : false; }

	static T[string] parseConfigAs(T)(string configName) if(is(T == struct))
	{
		T[string] retval;
		foreach(string name, Node value; configFiles[configName])
			retval[name] = yamlToStruct!T(value);
		return retval;
	}

	static Node rawConfig(string name)
	{
		return configFiles[name];
	}
}

T[] yamlToArray(T)(Node yaml)
{
	if(yaml.type != NodeType.sequence) return [];

	auto result = appender!(T[]);

	foreach(Node val; yaml)
	{
		static if(is(T == struct))
		{
			result.put(yamlToStruct!T(val));
		}
		else static if(is(T : string))
		{
			try
			{
				result.put(val.as!T);
			}
			catch(NodeException e)
			{
				sfmlishLogger.errorf(`YAMLConfig: Could not fit %s of type %s into an array of %s.`, val, val.nodeTypeString, T.stringof);
			}
		}
		else static if(is(T == R[], R))
		{
			result.put(yamlToArray!R(val));
		}
		else static if(is(T == R[string], R))
		{
			result.put(yamlToAssocArray!R(val));
		}
		else
		{
			try
			{
				result.put(val.as!T);
			}
			catch(NodeException e)
			{
				sfmlishLogger.errorf(`YAMLConfig: Could not fit %s of type %s into an array of %s.`, val, val.nodeTypeString, T.stringof);
			}
		}
	}

	return result.data;
}

T yamlToStruct(T)(Node yaml)
if(is(T == struct))
{
	if(yaml.type != NodeType.mapping) return T();

	T retval = T();
	static foreach(member; FieldNameTuple!T)
	{
		if(yaml.containsKey(member))
		{
			alias symbol = __traits(getMember, T, member);
			alias symbolType = typeof(symbol);

			static if(is(symbolType == struct))
			{
				mixin(`retval.` ~ member) = yamlToStruct!symbolType(yaml[member]);
			}
			else static if(is(symbolType : string))
			{
				try
				{
					mixin(`retval.` ~ member) = yaml[member].as!string;
				}
				catch(NodeException e)
				{
					sfmlishLogger.errorf(`Config: Could not fit %s of type %s into %s.%s of type %s`, yaml[member], yaml[member].nodeTypeString, T.stringof, member, symbolType.stringof);
				}
			}
			else static if(is(symbolType == R[], R))
			{
				mixin(`retval.` ~ member) = yamlToArray!R(yaml[member]);
			}
			else static if(is(symbolType == R[string], R))
			{
				mixin(`retval.` ~ member) = yamlToAssocArray!R(yaml[member]);
			}
			else
			{
				try
				{
					mixin(`retval.` ~ member) = yaml[member].as!symbolType;
				}
				catch(NodeException e)
				{
					sfmlishLogger.errorf(`Config: Could not fit %s of type %s into %s.%s of type %s`, yaml[member], yaml[member].nodeTypeString, T.stringof, member, symbolType.stringof);
				}
			}
		}
	}
	return retval;
}

T[string] yamlToAssocArray(T)(Node yaml)
{
	if(yaml.type != NodeType.mapping) return null;

	T[string] result;

	foreach(string key, Node val; yaml)
	{
		static if(is(T == struct))
		{
			result[key] = yamlToStruct!T(val);
		}
		else static if(is(T : string))
		{
			try
			{
				result[key] = val.as!T;
			}
			catch(NodeException e)
			{
				sfmlishLogger.errorf(`YAMLConfig: Could not fit %s of type %s into an array of %s.`, val, val.nodeTypeString, T.stringof);
			}
		}
		else static if(is(T == R[], R))
		{
			result[key] = yamlToArray!R(val);
		}
		else static if(is(T == R[string], R))
		{
			result[key] = yamlToAssocArray!R(val);
		}
		else
		{
			try
			{
				result[key] = val.as!T;
			}
			catch(NodeException e)
			{
				sfmlishLogger.errorf(`YAMLConfig: Could not fit %s of type %s into an array of %s.`, val, val.nodeTypeString, T.stringof);
			}
		}
	}

	return result;
}
