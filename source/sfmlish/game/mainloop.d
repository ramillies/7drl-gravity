module sfmlish.game.mainloop;

import sfmlish.game.appwindow;

import std.algorithm;
import std.range;

class Mainloop
{
	static private AppWindow[] windows;

	static void addWindow(AppWindow w)
	{
		windows ~= w;
	}

	static void run()
	{
		while(!windows.empty)
		{
			foreach(win; windows)
				win.loopStep;
			windows = windows.remove!`!a.isOpen`;
		}
	}
}
