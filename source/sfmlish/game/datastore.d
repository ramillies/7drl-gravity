module sfmlish.game.datastore;

import sfmlish.system.inputstream;

import std.file;
import std.path;
import std.format;
import std.zip;

interface GameDataReader
{
	InputStream getFile(string path);
	bool exists(string path);
}

class GameData
{
	private static GameDataReader reader;

	static void initialize(GameDataReader r) { reader = r; }
	static InputStream getFileStream(string path) { return reader.getFile(path); }
	static ubyte[] readFile(string path) { return getFileStream(path).slurp; }
	static string readFileAsUTF8(string path) { return getFileStream(path).slurpUTF8; }
	static bool exists(string path) { return reader.exists(path); }
}

class GameDataDirectory: GameDataReader
{
	private string path;

	this(string directory)
	{
		path = directory;
		if(!directory.isDir)
			throw new FileException(format(`The game data directory is supposed to be '%s' but it is not a directory.`, path));
	}

	override InputStream getFile(string filePath)
	{
		return new FileInputStream(buildPath(path, filePath));
	}

	override bool exists(string filePath)
	{
		string resultPath = buildPath(path, filePath);
		return resultPath.exists && !resultPath.isDir;
	}
}

class GameDataZip: GameDataReader
{
	private ZipArchive zip;

	this(string zipPath)
	{
		zip = new ZipArchive(read(zipPath));
	}

	override InputStream getFile(string path)
	{
		if(!exists(path))
			throw new FileException(format(`The path '%s' is not present in the game data ZIP.`, path));
		auto file = zip.directory[path];
		if(file.expandedData is null)
			zip.expand(file);
		return new MemoryInputStream(file.expandedData);
	}

	override bool exists(string path) { return (path in zip.directory) !is null; }
}

unittest
{
	// Create a test ZIP file
	import std.file;
	import std.string: representation;
	import std.exception: assertThrown;
	
	auto a = new ZipArchive();
	auto f = new ArchiveMember();
	with(f)
	{
		name = "blah";
		expandedData = cast(ubyte[]) "Some data, blah blah";
		compressionMethod = CompressionMethod.deflate;
	}
	a.addMember(f);
	write(deleteme, cast(byte[]) a.build());

	auto reader = new GameDataZip(deleteme);
	assert(reader.exists("blah"));
	assert(!reader.exists("quack"));
	assert(reader.getFile("blah").slurpUTF8 == "Some data, blah blah");
	// Try it twice, because it's not really clear what happens if zip.expand is called twice
	assert(reader.getFile("blah").slurpUTF8 == "Some data, blah blah");
	assertThrown(reader.getFile("quack"));

	std.file.remove(deleteme);
}
