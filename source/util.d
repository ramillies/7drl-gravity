import std.algorithm;
import sfmlish.system.vec;

bool rectsIntersecting(Vec2[] first, Vec2[] second)
{
	bool halfCheck(Vec2[] z, Vec2[] w)
	{
		return (
			w.map!((v) => (v - z[0]).dot(z[1] - z[0]) < 0).all ||
			w.map!((v) => (v - z[1]).dot(z[0] - z[1]) < 0).all ||
			w.map!((v) => (v - z[3]).dot(z[0] - z[3]) < 0).all ||
			w.map!((v) => (v - z[0]).dot(z[3] - z[0]) < 0).all
		);
	}
	
	return !(halfCheck(first, second) || halfCheck(second, first));
}

