HitPoints:
  defaults: [ 1, 1 ]
  structure: { current: 0, max: 1 }
  format: "{0} / {1}"
  description: "Hitpoints of an object (current and maximum). Objects without hitpoints aren't destructible by normal means."
  dialog:
    types: [ "%i", "%i" ]
    labels: [ "Current", "Maximum" ]
GravityCharge:
  defaults: [ 1.0 ]
  structure: { charge: 1 }
  format: "{0}"
  description: "Specifies how much is the object affected by gravity. If equal to zero or missing, the object is not affected at all; if negative, it is repulsed instead of attracted."
  dialog:
    types: ["%R"]
    labels: ["Charge"]
GravityWell:
  defaults: [ 1.0, 1.0 ]
  structure: { power: 0, decay: 1 }
  format: "{0} - {1} per sec"
  description: "Makes the object into a source of gravity. 'Power' is the power of the source and should be between 0 and 100. 'Decay' specifies how much power the source loses per second. When 'Power' drops to or below 0, this component is removed."
  dialog:
    types: [ "%R[0,100]", "%R" ]
    labels: [ "Power", "Decay" ]
MovementSpeed:
  defaults: [ 100.0 ]
  structure: { speed: 0 }
  format: "{0} pixels/sec"
  description: "Speed of the movement that the entity can perform of its own accord (in pixels per second)."
  dialog:
    types: [ "%R[0]" ]
    labels: [ "Speed" ]
Transparency:
  defaults: [ 0.0 ]
  structure: { transparency: 0 }
  format: "{0}"
  description: "Makes the object's sprite partially transparent. Transparency can range from 0 (opaque) to 1 (invisible)."
  dialog:
    types: [ "%R[0,1]" ]
    labels: [ "Transparency" ]
Colorization:
  defaults: [ 255, 255, 255 ]
  structure: { color: { r: 0, g: 1, b: 2 } }
  format: "{0} {1} {2}"
  description: "Colorizes the sprite of the object with the color given as RGB. The three components can go from 0 to 255."
  dialog:
    types: [ "%i[0,255]", "%i[0,255]", "%i[0,255]" ]
    labels: [ "R", "G", "B" ]
Player:
  defaults: [ ]
  structure: { }
  format: ""
  description: "Labels the object as a player. There should be only one player in the whole game."
  dialog:
    types: [ ]
    labels: [ ]
Monster:
  defaults: [ ]
  structure: { }
  format: ""
  description: "Labels the object as a monster."
  dialog:
    types: [ ]
    labels: [ ]
Missile:
  defaults: [ ]
  structure: { }
  format: ""
  description: "Labels the object as a missile. The game only checks for collisions between missiles and player/monsters."
  dialog:
    types: [ ]
    labels: [ ]
Wall:
  defaults: [ ]
  structure: { }
  format: ""
  description: "Labels the object as a wall."
  dialog:
    types: [ ]
    labels: [ ]
RotateInMoveDirection:
  defaults: [ ]
  structure: { }
  format: ""
  description: "Indicates that the sprite of this objects should be automatically rotated in the direction of its movement."
  dialog:
    types: [ ]
    labels: [ ]
AttackSimple:
  defaults: [ "", 2.0 ]
  structure: { projectile: 0, cooldown: 1 }
  format: "{0} each {1} s"
  description: "Objects with this component will shoot single shots at the player. 'Projectile' is the entity that is shot as a projectile; it will be shot with speed given by its own MovementSpeed in the player's direction. 'Cooldown' indicates how many seconds must pass between two attacks."
  dialog:
    types: [ "object", "%R[0]" ]
    labels: [ "Projectile", "Cooldown" ]
AttackFan:
  defaults: [ "", 2.0 ]
  structure: { projectile: 0, cooldown: 1 }
  format: "{0} each {1} s"
  description: "Objects with this component will shoot fans of three identical projectiles at the player (one directly at the player, one to the left and one to the right). 'Projectile' is the entity that is shot as a projectile; it will be shot with speed given by its own MovementSpeed. 'Cooldown' indicates how many seconds must pass between two attacks."
  dialog:
    types: [ "object", "%R[0]" ]
    labels: [ "Projectile", "Cooldown" ]
AttackCircular:
  defaults: [ "", 8, 2.0 ]
  structure: { projectile: 0, count: 1, cooldown: 2 }
  format: "{1}× {0} each {2} s"
  description: "Objects with this component will shoot identical projectiles all around itself (there will be 'Count' projectiles spaced evenly). 'Projectile' is the entity that is shot as a projectile; it will be shot with speed given by its own MovementSpeed. 'Cooldown' indicates how many seconds must pass between two attacks."
  dialog:
    types: [ "object", "%i[4]", "%R[0]" ]
    labels: [ "Projectile", "Count", "Cooldown" ]
AttackMultiple:
  defaults: [ "", 5, 0.2, 2.0 ]
  structure: { projectile: 0, count: 1, interval: 2, cooldown: 3 }
  format: "{1}× {0} each {3} s, with {2} s in between {0}'s"
  description: "Objects with this component will shoot 'Count' identical projectiles at the player in rapid succession (with 'Interval' seconds between each two). 'Projectile' is the entity that is shot as a projectile; it will be shot with speed given by its own MovementSpeed. 'Cooldown' indicates how many seconds must pass between two attacks."
  dialog:
    types: [ "object", "%i[1]", "%R[0]", "%R[0]" ]
    labels: [ "Projectile", "Count", "Interval", "Cooldown" ]
AttackBlocked:
  defaults: [ 0.0, false ]
  structure: { cooldown: 0, infinite: 1 }
  format: "{0} s, infinite={1}"
  description: "Indicates that the entity's attacks are blocked for the amount of seconds given in 'Cooldown'. As soon as it gets to or below zero, it is removed. If 'Infinite' is checked, the attack is blocked permanently."
  dialog:
    types: [ "%R[0]", "%b" ]
    labels: [ "Cooldown", "Infinite" ]
Hitbox:
  defaults: [ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ] 
  structure: { vertices: [ {x: 0, "y": 1}, {x: 2, "y": 3}, {x: 4, "y": 5}, {x: 6, "y": 7} ] }
  format: "(...)"
  description: "Specifies the hitbox of an object. The 'Graphic' component must be added before this one."
  dialog:
    special: hitbox
Graphic:
  defaults: [ "", 0, 0.0, 0.0 ]
  structure: { texture: 0, tile: 1, origin: { x: 2, "y": 3 } }
  format: "texture {0}, tile {1} (...)"
  description: "Specifies the picture used for the object. A texture and a tile can be picked graphically, as well as the 'Origin'. 'Origin' should be generally somewhere in the center of what is actually drawn in the picture. Any time when an object needs to be placed on a position, it is placed with its origin on the given position."
  dialog:
    special: graphic
Alternate:
  defaults: [ "", "", 0.0, 0.0 ]
  structure: { first: 0, second: 1, interval: 2, cooldown: 3 }
  format: "{0} and {1} with interval {2} s & cooldown {3} s"
  description: "Adds components bundled in 'First set' to the object. Then waits for 'Interval' seconds. Then removes all the components in the 'First set' and adds components in the 'Second set'. Then waits for 'Cooldown' seconds. These four steps are repeated indefinitely."
  dialog:
    types: [ "object", "object", "%R[0]", "%R[0]" ]
    labels: [ "First set", "Second set", "Interval", "Cooldown" ]
OnCollision:
  defaults: [ "", "" ]
  structure: { give-to-self: 0, give-to-other: 1 }
  format: "give {0} to self and {1} to other"
  description: "This should only be added to Missiles (it will have no effect on other objects). When the missile hits something, it gives the specified components (those bundled in the object 'Give to self') to itself, and those bundled in the object 'Give to other' to the object it hits. Typical setup will use SelfDestruct for the missile and Wounds for the target."
  dialog:
    types: [ "object", "object" ]
    labels: [ "Give to self", "Give to other" ]
OnCollisionWith:
  defaults: [ "", "", "" ]
  structure: { pattern: 0, give-to-self: 1, give-to-other: 2 }
  format: "{0}, give {1} to self and {2} to other"
  description: "This specifies what happens when the object hits another object. In such a case, the components bundled in 'Give to self' will be added to this object, and those in 'Give to other' will be added to the hit object. However, this will only happen if the other object has all the components bundled in 'Condition' (it needs to have the same types and the same values)."
  dialog:
    types: [ "object", "object", "other" ]
    labels: [ "Condition", "Give to self", "Give to other" ]
OnNear:
  defaults: [ "", 0.0, "", "" ]
  structure: { pattern: 0, distance: 1, give-to-self: 2, give-to-other: 3 }
  format: "{0} to distance {1}: give {2} to self and {3} to other"
  description: "This specifies what happens when the object comes near (to the 'Distance' or shorter) another object. In such a case, the components bundled in 'Give to self' will be added to this object, and those in 'Give to other' will be added to the hit object. However, this will only happen if the other object has all the components bundled in 'Condition' (it needs to have the same types and the same values)."
  dialog:
    types: [ "object", "%R[0]", "object", "object" ]
    labels: [ "Condition", "Distance", "Give to self", "Give to other" ]
OnDeath:
  defaults: [ "" ]
  structure: { give-to-self: 0 }
  format: "give {0} to self"
  description: "This specifies what happens when the object dies (its HP gets below 1 or it SelfDestructs). In such a case, components bundled in 'Give to self' will be added to it."
  dialog:
    types: [ "object" ]
    labels: [ "Give to self" ]
Mark:
  defaults: [ ]
  structure: { }
  format: ""
  description: "A mark that is automatically filled with the identity of the object that inflicts it. Can be used for more advanced algorithms."
  dialog:
    types: [ ]
    labels: [ ]
MoveTowardsNearest:
  defaults: [ "" ]
  structure: { pattern: 0 }
  format: "{0}"
  description: "This object will be always moving (with its 'MovementSpeed') towards the nearest object that has the same components as the object given in 'Condition'."
  dialog:
    types: [ "object" ]
    labels: [ "Condition" ]
Wounds:
  defaults: [ 1, 0.0 ]
  structure: { damage: 0, delay: 1 }
  format: "{0} after {1} s"
  description: "It only makes sense to add this component to other components that are inflicted on collisions etc. If this component is found on an object, it will be removed and the object loses the 'Wounds inflicted' amount of HP. Optionally, this can be delayed by setting the parameter 'Delay'."
  dialog:
    types: [ "%i[1]", "%R[0]" ]
    labels: [ "Wounds inflicted", "Delay" ]
SelfDestruct:
  defaults: [ 0.0 ]
  structure: { delay: 0 }
  format: "after {1} s"
  description: "Destroys the object after the given 'Delay'. This will trigger the OnDeath component."
  dialog:
    types: [ "%R[0]" ]
    labels: [ "Delay" ]
Vaporize:
  defaults: [ 0.0 ]
  structure: { delay: 0 }
  format: "after {1} s"
  description: "Destroys the object after the given 'Delay'. The object is simply removed without any other events."
  dialog:
    types: [ "%R[0]" ]
    labels: [ "Delay" ]
Shield:
  defaults: [ 1 ]
  structure: { hp: 0 }
  format: "{0} hp"
  description: "This component gives additional HP to its object. It is meant to be used as a support bonus."
  dialog:
    types: [ "%i[1]" ]
    labels: [ "Wounds blocked" ]
Supporter:
  defaults: [ "" ]
  structure: { pattern: 0 }
  format: "{0}"
  description: "This object will try to support other monsters, picking one each time and giving it the components packed in 'Bonuses'."
  dialog:
    types: [ "object" ]
    labels: [ "Bonuses" ]
Invisible:
  defaults: [ 0.9, 0.5 ]
  structure: { transparency: 0, cooldown: 1 }
  format: "{0} with cooldown {1} s"
  description: "This object will be \"invisible\" (rendered with given 'Transparency': 0 means fully visible, 1 means fully transparent) unless it has recently attacked. It takes 'Cooldown' seconds for the invisibility to activate after each attack."
  dialog:
    types: [ "%R[0,1]", "%R[0]" ]
    labels: [ "Transparency", "Cooldown" ]
Blinded:
  defaults: [ 0.9, 0.45 ]
  structure: { power: 0, decay: 1 }
  format: "intensity {0} minus {1} per sec"
  description: "If this component is put on a monster, it will be immobilized until it is removed. If it is put on a player, the whole screen will be covered with smoke animation ('Power' = 1 means the smoke is fully opaque, 0 means fully transparent). 'Power' decays by 'Decay' each second. As soon as 'Power' hits zero, the component is removed."
  dialog:
    types: [ "%R[0,1]", "%R[0]" ]
    labels: [ "Power", "Decay" ]
MoveAroundObject:
  defaults: [ "", 200.0 ]
  structure: { condition: 0, radius: 1 }
  format: "{0} in distance {1}"
  description: "This object will try to move (with its own MovementSpeed) around the nearest object that has the components bundled in 'Condition', trying to keep at least 'Radius' pixels away from it."
  dialog:
    types: [ "object", "%R[0]" ]
    labels: [ "Condition", "Radius" ]
Spin:
  defaults: [ 50.0 ]
  structure: { velocity: 0 }
  format: "{0} degrees per second"
  description: "This object will continuously rotate around its origin with given 'Angular Velocity' (in degrees per second)."
  dialog:
    types: [ "%R[0]" ]
    labels: [ "Angular Velocity" ]
SpeedUp:
  defaults: [ 2.0 ]
  structure: { factor: 0 }
  format: "{0} times"
  description: "This component is meant to be assigned as a bonus. It will speed up all movement and attack by the given 'Factor' (1 means no change, so 2 = everything is twice as fast, 0.5 = twice as slow)."
  dialog:
    types: [ "%R[0]" ]
    labels: [ "Factor" ]
LimitedRange:
  defaults: [ 200.0, "" ]
  structure: { distance: 0, onStop: 1 }
  format: "{0} px, do {1} on reaching the maximum"
  description: "This component is meant for shots that will fly only certain distance. After the object has moved 'Max Distance' in total, components bundled in 'On Reach Max' will be added to it."
  dialog:
    types: [ "%R[0]", "object" ]
    labels: [ "Max Distance", "On Reach Max" ]
FastenedTo:
  defaults: [ "", 200.0 ]
  structure: { pattern: 0, distance: 1 }
  format: "{0}, max length {1}"
  description: "The object may not move from the nearest object that matches all the components in 'Condition' further than 'Distance'. If it will somehow get further, it will be immediately moved so that the limit is fulfilled."
  dialog:
    types: [ "object", "%R[0]" ]
    labels: [ "Condition", "Distance" ]
Blink:
  defaults: [ 200.0, 1.0 ]
  structure: { distance: 0, cooldown: 1 }
  format: "distance {0} px with cooldown {1} s"
  description: "This object will teleport short distance (up to 'Distance') instead of movement. It will do so each 'Cooldown' seconds."
  dialog:
    types: [ "%R[0]", "%R[0]" ]
    labels: [ "Distance", "Cooldown" ]
RandomShove:
  defaults: [ 100., 50. ]
  structure: { power: 0, decay: 1 }
  format: "{0} px/sec minus {1} px/sec per sec"
  description: "When this component is put on an object, it is removed and the object is shoved in random direction with given 'Speed' that decays with given 'Decay' (in pixels/sec per second)."
  dialog:
    types: [ "%R[0]", "%R[0]" ]
    labels: [ "Speed", "Decay" ]
Remove:
  defaults: [ [ ] ]
  structure: { components: 0 }
  format: "{0}"
  description: "When this component is encountered on an object, it is removed, along with all specified 'Components'. Components are specified by name only."
  dialog:
    types: [ "strings" ]
    labels: [ "Component names to remove (one per line)" ]
Spawn:
  defaults: [ "" ]
  structure: { object: 0 }
  format: "{0}"
  description: "As soon as this compomnent is encountered on an object, it is removed and the given 'Object' is spawned on the same place. This is not limited by any cooldowns and it does not trigger any other events."
  dialog:
    types: [ "object" ]
    labels: [ "Object" ]
CustomMark:
  defaults: [ 0 ]
  structure: { type: 0 }
  format: "{0}"
  description: "This is a special marker for some advanced effects. The 'ID' just differentiates the type of the mark; it can be any integer. This mark is not filled with identity of any object."
  dialog:
    types: [ "%i" ]
    labels: [ "ID" ]
