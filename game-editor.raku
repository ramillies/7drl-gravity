#!env raku

use IUP;
use IUP::Consts;
use YAMLish;
use NativeCall;

IUP.open;

IUP<utf8mode> = True;

my $data-dir;

my $texture-info;
my $selected-texture;

class TileCanvas
{
	has $.image;
	has $.scale;
	has $.canvas;
	has $.texture-name;

	has $!selected-tile = Any;

	method load-texture-by-name($name)
	{
		$!texture-name = $name;
		my $info = $texture-info{$name};
		my $path = ~ $data-dir.IO.add($info<path>);
		$!image = IUP.load-image: $path;

		my ($cw, $ch) = $!canvas<rastersize>.split("x");
		$!scale = [min] $cw/$!image<width>, $ch/$!image<height>, 5;
		$!image<resize> = "{($!image<width>*$!scale).Int}x{($!image<height>*$!scale).Int}";

		return "Image: {$info<path>}. Size: {$!image<originalscale>}";
	}

	method redraw { $!canvas.update; }

	method select-tile($n) { $!selected-tile = $n; }
	method deselect-tile { $!selected-tile = Any; }

	method xy-to-tile($x, $y)
	{
		my ($orig-w, $orig-h) = $!image<originalscale>.split("x");
		my ($cell-w, $cell-h) = $texture-info{$!texture-name}<tileSize> // ($orig-w, $orig-h);
		my ($stride-x, $stride-y) = $cell-w * $!scale, $cell-h * $!scale;
		for (0...$orig-h/$cell-h - 1) X (0...$orig-w/$cell-w - 1) -> ($row, $col)
		{
			state $n = 0;
			if ($texture-info{$!texture-name}<tileCount> // 1) > $n && $col <= $x/$stride-x < $col+1 && $row <= $y/$stride-y < $row+1
			{
				return $n;
			}
			$n++;
		}
		return Nil;
	}

	method set-canvas-action
	{
		$!canvas.on-action: -> *%_ {
			with $!image
			{
				given $!canvas
				{
					.draw-start;
					.draw-parent-background;
					.draw-image: $!image, [0,0];
					my ($orig-w, $orig-h) = $!image<originalscale>.split("x");
					my ($cell-w, $cell-h) = $texture-info{$!texture-name}<tileSize> // ($orig-w, $orig-h);
					my ($stride-x, $stride-y) = $cell-w * $!scale, $cell-h * $!scale;
					.<drawfont> = .<font>.subst(/\d + $/, -($stride-x/5 max 20).Int);
					my @selection-box;
					for (0...$orig-h/$cell-h - 1) X (0...$orig-w/$cell-w - 1) -> ($row, $col)
					{
						state $n = 0;
						.<drawcolor> = "255 0 0";
						if ($texture-info{$!texture-name}<tileCount> // 1) > $n
						{
							.draw-rect: [$col*$stride-x, $row*$stride-y], [($col+1)*$stride-x, ($row+1)*$stride-y];
							if $!selected-tile.defined && $!selected-tile == $n
							{
								@selection-box = [$col*$stride-x, $row*$stride-y], [($col+1)*$stride-x, ($row+1)*$stride-y];
								.<drawcolor> = "255 255 0";
							}
							.draw-text: "$n", [$col*$stride-x, $row*$stride-y];
						}
						$n++;
					}
					.<drawcolor> = "255 0 255";
					.draw-rect: [0,0], $!image<width height>;
					.<drawcolor> = "255 255 0";
					.draw-rect: |@selection-box if @selection-box.elems == 2;
					.draw-end;
				}
			}
			Default;
		}
	}
}


my %texture-tab = load => IUP.button("Load"),
	save => IUP.button("Save"),
	list => IUP.list(:expand),
	canvas => IUP.canvas(:expand),
	set-image => IUP.button("Load image"),
	rename => IUP.button("Rename texture"),
	delete => IUP.button("Delete texture"),
	size-x => IUP.text,
	size-y => IUP.text,
	tile-count => IUP.text,
	status => IUP.label("Nothing"),
;

my $texture-canvas = TileCanvas.new(canvas => %texture-tab<canvas>);
$texture-canvas.set-canvas-action;

my $object-info;
my $selected-object;
my $current-component-list;
my $components = load-yaml "component-list.yaml".IO.slurp;

my %object-tab = load => IUP.button("Load"),
	save => IUP.button("Save"),
	objects => IUP.list(:expand),
	pieces => IUP.list(:expand)
;

my $tabs = IUP.tabs(
	IUP.vbox(
		IUP.hbox(|%texture-tab<load save>),
		IUP.split(%texture-tab<list>,
			IUP.vbox(
				%texture-tab<canvas>,
				IUP.hbox(|%texture-tab<set-image rename delete>, IUP.label("Tile size: "), %texture-tab<size-x>, IUP.label("×"), %texture-tab<size-y>, IUP.label("Tile count: "), %texture-tab<tile-count>),
				%texture-tab<status>
			),
			value => 150
		)
	),
	IUP.vbox(
		IUP.hbox(|%object-tab<load save>),
		IUP.split(%object-tab<objects>, %object-tab<pieces>, value => 150)
	)
);

%texture-tab<load>.on-action: -> *%_ {
	my $open-dialog = IUP.file-dialog(filter => "*.yaml", directory => $data-dir);
	$open-dialog.popup;
	if $open-dialog<status> == 0
	{
		$texture-info = load-yaml $open-dialog<value>.IO.slurp;
		%texture-tab<list>.items = |$texture-info.keys.sort, "(Add new...)";
	}
	Default;
};

%texture-tab<save>.on-action: -> *%_ {
	my $dialog = IUP.file-dialog(dialogtype => "SAVE", extdefault => "yaml", filter => "*.yaml", directory => $data-dir);
	$dialog.popup;
	if $dialog<status> != -1
	{
		say $dialog;
		say $texture-info;
		$dialog<value>.IO.spurt: save-yaml $texture-info;
	}
	Default;
};

%texture-tab<list>.on-action: -> *%_ {
	say %_;
	my $list = %_<source>;
	say $list.elems;
	my $cancelled = False;
	if %_<selected>
	{
		if %_<index> == $list.elems-1
		{
			# Add new texture
			my $answers = IUP.get-param: "New texture", "Texture name: %s\nFilename: \%f[OPEN|*.jpg;*.png|$data-dir||]\n", ["", ""];
			if $answers<button> == 1
			{
				my ($name, $file) = $answers<values>;
				if $name ~~ "" || ($texture-info{$answers<values>[0]}:exists)
				{
					IUP.message-dialog(dialogtype => "WARNING", value => "The texture name is invalid. It needs to be non-empty and it must not be already used.").popup;
				}
				elsif !$file.IO.f
				{
					IUP.message-dialog(dialogtype => "WARNING", value => "You need to choose an existing image as a texture.").popup;
				}
				else
				{
					$list.insert: $name, :before($list.elems-1);
					$texture-info{$name} = %( path => $file.IO.relative($data-dir.IO) );
					$list.selection = $list.elems-2;
				}
			}
			else
			{
				$cancelled = True;
			}

		}
		unless $cancelled
		{
			$selected-texture = $list[$list.selection]<title>;
			my $info = $texture-info{$selected-texture};
			%texture-tab<status><title> = $texture-canvas.load-texture-by-name($selected-texture);
			$texture-canvas.redraw;
			my ($orig-w, $orig-h) = $texture-canvas.image<originalscale>.split("x");
			%texture-tab<size-x><value> = $info<tileSize>[0] // $orig-w;
			%texture-tab<size-y><value> = $info<tileSize>[1] // $orig-h;
			%texture-tab<tile-count><value> = $info<tileCount> // 1;
		}
	}
}


%texture-tab<set-image>.on-action: -> *%_ {
	my $open-dialog = IUP.file-dialog(directory => $data-dir, extfilter => "Images|*.png;*.jpg|Anything|*.*");
	$open-dialog.popup;
	if $open-dialog<status> == 0
	{
		$texture-info{$selected-texture}<path> = $open-dialog<value>.IO.relative($data-dir.IO);
		$texture-canvas.redraw;
	}
	Default;
};

%texture-tab<rename>.on-action: -> *%_ {
	my $result = IUP.get-param("Rename texture", "New name:%s\n", [""]);
	my $new-name = $result<values>[0];
	if $result<button> == 1
	{
		if $new-name ~~ ""
		{
			IUP.message-dialog(dialogtype => "WARNING", value => "The name needs to be non-empty.").popup;
		}
		elsif $texture-info{$new-name}:exists
		{
			IUP.message-dialog(dialogtype => "WARNING", value => "This name is already being used. Pick a different one.").popup;
		}
		else
		{
			$texture-info{$new-name} = $texture-info{$selected-texture};
			$texture-info{$selected-texture}:delete;
			$selected-texture = $new-name;
			my $list-sel = %texture-tab<list>.selection;
			%texture-tab<list>[$list-sel] = $new-name;
			%texture-tab<list>.selection = $list-sel;
		}
	}
	Default;
}

%texture-tab<delete>.on-action: -> *%_ {
	my $result = IUP.alarm: "Delete texture", "Do you really want to delete the texture '$selected-texture'?", "Yes", "No";
	if $result == 1
	{
		$texture-info{$selected-texture}:delete;
		%texture-tab<list>.remove: %texture-tab<list>.selection;
		%texture-tab<list>.update;
	}
	Default;
}

%texture-tab<size-x>.on-valuechanged: -> *%_ {
	if %_<source><value> ~~ /^ <[1..9]> \d* $/
	{
		$texture-info{$selected-texture}<tileSize> = [ +%_<source><value>, +%texture-tab<size-y><value> ];
		$texture-canvas.redraw;
	}
	else
	{
		%_<source><value> = $texture-info{$selected-texture}<tileSize>[0] // $texture-canvas.image<width>;
	}
	Default;
};

%texture-tab<size-y>.on-valuechanged: -> *%_ {
	if %_<source><value> ~~ /^ <[1..9]> \d* $/
	{
		$texture-info{$selected-texture}<tileSize> = [ +%texture-tab<size-x><value>, +%_<source><value> ];
		$texture-canvas.redraw;
	}
	else
	{
		%_<source><value> = $texture-info{$selected-texture}<tileSize>[1] // $texture-canvas.image<height>;
	}
	Default;
};

%texture-tab<tile-count>.on-valuechanged: -> *%_ {
	if %_<source><value> ~~ /^ <[1..9]> \d* $/
	{
		$texture-info{$selected-texture}<tileCount> = +%_<source><value>;
		$texture-canvas.redraw;
	}
	else
	{
		%_<source><value> = $texture-info{$selected-texture}<tileCount> // 1;
	}
	Default;
};

###########################################
######    OBJECT TAB
###########################################

class ComponentList { ... }

class Component
{
	has $.structure;
	has $.format;
	has @.data;
	has $.dialog-structure;
	has $.name;
	has $.description;

	sub two-sided-walk($a, $b)
	{
		gather for $a.kv -> $k, $v
		{
			if $v ~~ Int { take $v => $b{$k} }
			if $v ~~ Iterable { take two-sided-walk %($v.kv), %($b{$k}.kv) }
		}
	}

	sub parse-structure($a,$b) { (flat two-sided-walk %($a.kv), %($b.kv)).sort(+*.key).map(*.value).list }

	method load-data($x) { @.data = parse-structure($.structure, $x) Z// @.data }
	method put-data { $.structure.duckmap(-> Int $n { @.data[$n] }) }
	method gist { $.format.subst(/ '{' (\d+) '}' /, { @.data[$0].gist }, :g) }

	method new-from-spec($name, :$data)
	{
		say "Making {$components{$name}.raku}...";
		my $result = Component.new(
			:$name,
			structure => .<structure>,
			format => .<format>,
			data => @(.<defaults>),
			dialog-structure => .<dialog>,
			description => .<description>
		) given $components{$name};
		$result.load-data($data) with $data;
		return $result;
	}

	method fill-with-dialog(:$allow-delete = False)
	{
		CATCH { default { say .^name, ": ", .message } }
		if $.dialog-structure<special>:exists
		{
			if $!dialog-structure<special> ~~ "hitbox"
			{
				without $texture-info { IUP.message: "Error!", "You must load some textures before using this dialog."; return }
				my $canvas = IUP.canvas(rastersize => "128x128");
				my $ok-button = IUP.button("Set hitbox");
				my $cancel-button = IUP.button("Cancel");
				my $dialog = IUP.dialog(IUP.vbox($canvas,IUP.hbox($ok-button, $cancel-button)));
				$dialog.map;

				my ($image, $scale, @pos, @size, @origin);
				with $object-info{$selected-object}<Graphic>
				{
					my $name = .<texture>;
					my $info = $texture-info{$name};
					say $name;
					say $info;
					my $path = $data-dir.IO.add($info<path>).absolute;
					$image = IUP.load-image: $path;
					my ($orig-w, $orig-h) = $image<width height>;
					my ($cell-w, $cell-h) = $info<tileSize> // ($orig-w, $orig-h);

					my ($cw, $ch) = $canvas<rastersize>.split("x");
					$scale = [min] $cw/$cell-w, $ch/$cell-h, 5;
					$image<resize> = "{($image<width>*$scale).Int}x{($image<height>*$scale).Int}";

					my ($stride-x, $stride-y) = $cell-w * $scale, $cell-h * $scale;
					my $perRow = (+$orig-w) div +$cell-w;
					my ($row, $col) = .<tile> div $perRow, .<tile> mod $perRow;
					@pos = [ $col*$stride-x, $row*$stride-y];
					@size = $stride-x, $stride-y;
					@origin = .<origin><x y>;
				}
				else { IUP.message("Error!", "Hitbox can be only specified for object with 'Graphic' component."); return }
				my @corners = ((@!data >>+>> @origin) >>*>> $scale)[0,1,4,5];
				my $dragging = False;
				$canvas.on-button: -> *%_ {
					if %_<button> == 49
					{
						if %_<pressed>
						{
							@corners = %_<x y x y>;
						}
						$dragging = %_<pressed>
					}
					Default;
				}

				$canvas.on-motion: -> *%_ {
					if $dragging
					{
						@corners[2,3] = %_<x y>;
						$canvas.update;
					}
					Default;
				}

				$canvas.on-action: -> *%_ {
					given %_<source>
					{
						.draw-start;
						.draw-parent-background;
						.clip-rect = [ [0, 0], @size ];
						.draw-image: $image, @pos >>*>> -1;
						.draw-selection-rect: @corners[0,1], @corners[2,3];
						.draw-end;
					}
				}

				$ok-button.on-action: -> *%_ {
					@!data = ((@corners >>/>> $scale) >>->> @origin)[0,1,2,1,2,3,0,3];
					Close;
				}

				$cancel-button.on-action: -> *%_ { Close }

				$dialog.popup;
			}
			elsif $!dialog-structure<special> ~~ "graphic"
			{
				without $texture-info { IUP.message: "Error!", "You must load some textures before using this dialog."; return }
				my $tile-canvas = IUP.canvas(rastersize => "128x128");
				my $tileset-canvas = IUP.canvas(rastersize => "600x600");
				my $texture-list = IUP.list(:20visiblecolumns);
				my $ok-button = IUP.button("Set");
				my $cancel-button = IUP.button("Cancel");
				my $dialog = IUP.dialog(IUP.vbox(IUP.hbox($tileset-canvas, IUP.vbox($texture-list, $tile-canvas)), IUP.hbox($ok-button, $cancel-button)));
				$dialog.map;

				$texture-list.items = $texture-info.keys.sort;
				my $texture = @!data[0];
				my $tilenumber = @!data[1];
				my @origin = @!data[2,3];
				my ($tileset, $tile-image, $tile-scale, @tile-pos);
				my (@tile-stride, $tiles-per-row);
				$tileset = TileCanvas.new(canvas => $tileset-canvas);
				$tileset.set-canvas-action;

				sub redraw-tile
				{
					my ($row, $col) = $tilenumber div $tiles-per-row, $tilenumber mod $tiles-per-row;
					@tile-pos = ($col, $row) Z* @tile-stride;
					$tile-canvas.update;
				}

				sub redraw-everything
				{
					$tileset.load-texture-by-name: $texture;
					$tileset.select-tile: $tilenumber;
					$tileset.redraw;
					my $info = $texture-info{$texture};
					my $path = $data-dir.IO.add($info<path>).absolute;
					$tile-image = IUP.load-image: $path;
					my ($orig-w, $orig-h) = $tile-image<width height>;
					my ($cell-w, $cell-h) = $info<tileSize> // ($orig-w, $orig-h);

					my ($cw, $ch) = $tile-canvas<rastersize>.split("x");
					$tile-scale = [min] $cw/$cell-w, $ch/$cell-h, 5;
					$tile-image<resize> = "{($tile-image<width>*$tile-scale).Int}x{($tile-image<height>*$tile-scale).Int}";

					@tile-stride = $cell-w * $tile-scale, $cell-h * $tile-scale;
					$tiles-per-row = (+$orig-w) div +$cell-w;

					redraw-tile;
				}

				if $texture !~~ ""
				{
					say $texture-list.items.first({.<title> ~~ $texture}, :k);
					$texture-list<value> = 1+ $texture-list.items.first({.<title> ~~ $texture}, :k);
					redraw-everything;
				}

				$texture-list.on-action: -> *%_ {
					if %_<selected>
					{
						$texture = %_<text>;
						$tilenumber = 0;
						@origin = 0,0;
						redraw-everything;
					}
					Default;
				}

				$tileset-canvas.on-button: -> *%_ {
					if %_<button> == 49
					{
						with $tileset.xy-to-tile(|%_<x y>)
						{
							$tileset.select-tile: $_;
							$tilenumber = $_;
							redraw-tile;
							$tileset.redraw;
						}
					}
					Default;
				}

				$tile-canvas.on-button: -> *%_ {
					if %_<button> == 49
					{
						@origin = %_<x y> >>/>> $tile-scale;
						$tile-canvas.update;
					}
					Default;
				}

				$tile-canvas.on-action: -> *%_ {
					given %_<source>
					{
						.draw-start;
						.draw-parent-background;
						.clip-rect = [ [0, 0], @tile-stride ];
						.draw-image: $tile-image, @tile-pos >>*>> -1;
						.<drawcolor> = "255 0 0";
						my ($x, $y) = @origin >>*>> $tile-scale;
						.draw-arc: [$x-5, $y-5], [$x+5, $y+5];
						.draw-end;
					}
					Default;
				}

				$ok-button.on-action: -> *%_ {
					@!data = $texture, $tilenumber, |@origin;
					Close;
				}

				$cancel-button.on-action: -> *%_ { Close }

				$dialog.popup;
			}
			else { IUP.message: "Not Implemented", "Cannot use special dialog boxes right now."; }
			return;
		}

		if $.dialog-structure{"types" | "labels"}:!exists
		{
			IUP.message: "Bad dialog spec", "Wrong dialog specification.";
			return;
		}

		my @types = @($.dialog-structure<types>);
		my @labels = @($.dialog-structure<labels>);
		my @initials = @!data;
		my @all-objects = $object-info.keys.sort;

		my %additional-objects;
		for @types.kv -> $n, $type is rw
		{
			if $type ~~ "object"
			{
				$type = "%l|(no object)|" ~ @all-objects.join("|") ~ "|";
				@initials[$n] = 1 + (@all-objects.first(@!data[$n], :k) // -1);
			}
			if $type ~~ "strings"
			{
				$type = "%m";
				@initials[$n] = @!data[$n].join("\n");
			}
		}

		my $param-string = "Parameters for {$.name}: %t\n" ~ ($allow-delete ?? "Delete component? %b\n" !! "") ~ ((@labels Z=> @types).map({ "{.key}: {.value}\n" }).join);
		say $param-string;
		my $prompt = IUP.get-param: "Edit Component", $param-string, (($allow-delete ?? 0 !! Empty), |@initials);
		say $prompt;

		sub parse-results(@r)
		{
			gather for @r.kv -> $n, $value
			{
				if $!dialog-structure<types>[$n] ~~ "object"
				{
					take $value == 0 ?? "" !! @all-objects[$value-1];
				}
				elsif $!dialog-structure<types>[$n] ~~ "strings"
				{
					take @($value.split("\n"));
				}
				else { take $value }
			}
		}

		if $prompt<button> == 1
		{
			if $allow-delete
			{
				@.data = parse-results @($prompt<values>)[1..*-1];
				return so $prompt<values>[0];
			}

			@.data = parse-results @($prompt<values>);
		}
		say "New data: {@!data.gist}";
		return False;
	}	
}

class ComponentList
{
	has Component @.components;
	has IUP::List $.handle;

	submethod TWEAK()
	{
		@!components .= sort(*.name);
		$!handle.items = |(@!components.map({ "{.name} ({.gist})" })), "(Add new...)";
		$!handle.on-button: -> *%_ {
			if %_<button> == 49 && %_<double-click>
			{
				my $index = -1 + IupConvertXYToPos $!handle, |%_<x y>;
				if $index == @!components.elems
				{
					my $choices = IUP.list(:expand);
					my $help = IUP.text(:multiline, :readonly, :wordwrap, expand => "HORIZONTAL", :5visiblelines);
					$choices.items = ($components.keys (-) @!components.map(*.name)).keys.sort;
					$choices.on-action: -> *%_ {
						$help<value> = $components{%_<text>}<description> if %_<selected>;
						Default;
					}

					my $ok-button = IUP.button("Add component");
					$ok-button.on-action: -> *%_ {
						my $added = Component.new-from-spec($choices[$choices.selection]<title>);
						$added.fill-with-dialog;
						@!components.push: $added;
						Close;
					}

					my $cancel-button = IUP.button("Cancel");
					$cancel-button.on-action: -> *%_ { Close }

					IUP.dialog(IUP.vbox(
						$choices, $help,
						IUP.hbox($ok-button, $cancel-button)
					), size => "QUARTER").popup;
				}
				elsif $index >= 0
				{
					my $should-delete = @!components[$index].fill-with-dialog: :allow-delete;
					@!components.splice($index, 1) if $should-delete;
				}
				@!components .= sort(*.name);
				$!handle.items = |(@!components.map({ "{.name} ({.gist})" })), "(Add new...)";
				$object-info{$selected-object} = %(@!components.map({ .name => .put-data }));
			}
			Default;
		}
	}
	
	method load-components(%list)
	{
		CATCH { default { say .^name, ": ", .message } }
		@!components = %list.pairs.map({ Component.new-from-spec(.key, data => .value) });
		$.update-list;
	}

	method put-components { %(@!components.map({ .name => .put-data })) }

	method update-list
	{
		@!components .= sort(*.name);
		$!handle.items = |(@!components.map({ "{.name} ({.gist})" })), "(Add new...)";
		$object-info{$selected-object} = $.put-components;
	}
}

%object-tab<load>.on-action: -> *%_ {
	my $open-dialog = IUP.file-dialog(filter => "*.yaml", directory => $data-dir);
	$open-dialog.popup;
	if $open-dialog<status> == 0
	{
		$object-info = load-yaml $open-dialog<value>.IO.slurp;
		%object-tab<objects>.items = |$object-info.keys.sort, "(Add new...)";
	}
	Default;
};

%object-tab<save>.on-action: -> *%_ {
	my $dialog = IUP.file-dialog(dialogtype => "SAVE", extdefault => "yaml", filter => "*.yaml", directory => $data-dir);
	$dialog.popup;
	if $dialog<status> != -1
	{
		say $dialog;
		say $object-info;
		$dialog<value>.IO.spurt: save-yaml $object-info;
	}
	Default;
};

%object-tab<objects>.on-action: -> *%_ {
	say %_;
	my $list = %_<source>;
	say $list.elems;
	my $cancelled = False;
	if %_<selected>
	{
		if %_<index> == $list.elems-1
		{
			# Add new texture
			my $answers = IUP.get-param: "New object", "Object name: %s\n", [""];
			if $answers<button> == 1
			{
				my $name = $answers<values>[0];
				if $name ~~ "" || ($object-info{$name}:exists)
				{
					IUP.message-dialog(dialogtype => "WARNING", value => "The object name is invalid. It needs to be non-empty and it must not be already used.").popup;
				}
				else
				{
					$list.insert: $name, :before($list.elems-1);
					$object-info{$name} = %();
					$list.selection = $list.elems-2;
				}
			}
			else { $cancelled = True; }

		}
		unless $cancelled
		{
			$selected-object = $list[$list.selection]<title>;
			say $object-info{$selected-object};
			$current-component-list = ComponentList.new(handle => %object-tab<pieces>);
			$current-component-list.load-components: $object-info{$selected-object};
		}
	}
}

$tabs<TABTITLE0> = "Textures";
$tabs<TABTITLE1> = "Objects";

my $win = IUP.dialog($tabs, size => "FULL");
$win.show;

my $choose-dir = IUP.file-dialog(dialogtype => "DIR", title => "Choose the game's data directory");
$choose-dir.popup;

if $choose-dir<status> != 0
{
	IUP.message-dialog(value => "You must select the game data directory!", dialogtype => "ERROR").popup;
	exit;
}

$data-dir = $choose-dir<value>;

IUP.mainloop;
IUP.close;
